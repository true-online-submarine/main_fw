# Read more here: 
# 1.https://freertos.org/FreeRTOS_Support_Forum_Archive/May_2019/freertos_selected_processor_does_not_support_vstmdbeq_r0_s16-s31_in_Thumb_mode_7a5083cd73j.html
# 2. https://community.platformio.org/t/error-object-uses-vfp-register-arguments-firmware-elf-does-not/25263/2
#
# https://docs.platformio.org/en/latest/scripting/index.html

Import("env")
flags = [
   "-mfloat-abi=hard",
   "-mfpu=fpv4-sp-d16"
]
env.Append(CCFLAGS=flags, LINKFLAGS=flags)
