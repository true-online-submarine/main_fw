#include "main.h"
#include "stm32f4xx_it.h"
#include "piston_range_sensor_adapter.h"
#include "depth_laser_sensor_adapter.h"
#include "banana_message_dispatcher.h"
#include "rf433_dispatcher.h"

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    /* VL53L0X generate gpio interrupt when data ready.
     * See piston_range_sensor_init() and vl53l0x_init() */
    if (GPIO_Pin == VL53L0X_INT_Pin) {
        piston_range_sensor_isr();
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    /* The timer is used only before RTOS scheduler start
     * to provide HAL_Delay() service */
    if (htim->Instance == TIM10) {
        HAL_IncTick();
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    /* When DS_TFMINIS_USE_UART_IRQ==1 this will handle byte from TFmini-S */
    if (huart->Instance == USART1) {
        depth_tof_lidar_uart_irq_handler();
    } else if (huart->Instance == USART6) { /* handle bytes from Linux on Banana Pi board */
        banana_message_dispatcher_handle_byte_isr();
    }
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    /* When DS_TFMINIS_USE_DMA_IDLE_IRQ==1 this will handle byte from TFmini-S */
    if (huart->Instance == USART1) {
        depth_tof_lidar_dma_irq_handler((size_t)Size);
    }
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM3 && htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4) {
        rf433_dispatcher_edge_ic_isr_handler();
    }
}
