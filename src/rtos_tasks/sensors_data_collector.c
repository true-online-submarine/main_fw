#include <stdlib.h>
#include "sensors_data_collector.h"
#include "main.h"

#include "FreeRTOS.h"
#include "task.h"

#include "sub_logger.h"
#include "sub_errors.h"

#include "depth_laser_sensor_adapter.h"
#include "piston_range_sensor_adapter.h"
#include "diving_surfacing_system_config.h"
#include "hx710b_mps20n0040d_adapter.h"

#define THREAD_OS_DELAY_MS (10UL)



/* Data and functiond to handle VL53L0X laser range sensor */
#if defined(DS_SYSTEM_PISTON_DETECTOR_USE_VL53L0X)
#if (DS_VL53L0X_LOGGER_USE == 1)
static int vl53l0_logging_cycles_cnt = 0;
static void vl53l0_logging_cycles_inc(void) { vl53l0_logging_cycles_cnt++; }
static void vl53l0_logging_cycles_reset(void) { vl53l0_logging_cycles_cnt = 0; }
static int vl53l0_logging_cycles_get(void) { return vl53l0_logging_cycles_cnt; }
#endif /* DS_VL53L0X_LOGGER_USE */

static int vl53l0_getting_data_cycles_cnt = 0;
static void vl53l0_getting_data_cycles_inc(void) { vl53l0_getting_data_cycles_cnt++; }
static void vl53l0_getting_data_cycles_reset(void) { vl53l0_getting_data_cycles_cnt = 0; }
static int vl53l0_getting_data_cycles_get(void) { return vl53l0_getting_data_cycles_cnt; }

static void process_laser_sensor_vl53l0x_data(void)
{
    if (vl53l0_getting_data_cycles_get() >= DS_VL53L0X_GETTING_DATA_PERIOD_MS / THREAD_OS_DELAY_MS) {
        piston_range_sensor_process();
        vl53l0_getting_data_cycles_reset();
    }

#if (DS_VL53L0X_LOGGER_USE == 1)
    if (vl53l0_logging_cycles_get() >= DS_VL53L0X_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
        logger_dgb_print("[collector][vl53l0x] piston position = %d mm\n", piston_range_sensor_get_pos_mm());
        vl53l0_logging_cycles_reset();
    }
#endif /* DS_VL53L0X_LOGGER_USE */
}
#endif /* DS_SYSTEM_PISTON_DETECTOR_USE_VL53L0X */



#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS)
# if (DS_TFMINIS_LOGGER_USE == 1)
static int tfminis_logging_cycles_cnt = 0;
static void tfminis_logging_cycles_inc(void) { tfminis_logging_cycles_cnt++; }
static void tfminis_logging_cycles_reset(void) { tfminis_logging_cycles_cnt = 0; }
static int tfminis_logging_cycles_get(void) { return tfminis_logging_cycles_cnt; }
# endif /* DS_TFMINIS_LOGGER_USE */

static void process_laser_depth_sensor_tfminis_data(void)
{
    if (depth_tof_lidar_is_new_data_available()) {
        tof_sensor_data_t d = depth_tof_lidar_get_distance_cm();
        uint16_t temperature = depth_tof_lidar_get_temp();

#if (DS_TFMINIS_LOGGER_USE == 1)
    if (tfminis_logging_cycles_get() >= DS_TFMINIS_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
        logger_dgb_print("[collector][TFmini-S] %dcm, e=%d, T=%dC\n",
                         d.distanse_cm, d.err_reason, temperature);
        tfminis_logging_cycles_reset();
    }
#endif
        }
}
#endif /* DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS */



#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_MPS20N0040D)
# if (DS_MPS20N0040D_LOGGER_USE == 1)
static int mps20n0040d_logging_cycles_cnt = 0;
static void mps20n0040d_logging_cycles_inc(void) { mps20n0040d_logging_cycles_cnt++; }
static void mps20n0040d_logging_cycles_reset(void) { mps20n0040d_logging_cycles_cnt = 0; }
static int mps20n0040d_logging_cycles_get(void) { return mps20n0040d_logging_cycles_cnt; }
# endif /* DS_MPS20N0040D_LOGGER_USE */

static void process_water_pressure_mps20n0040d_data(void)
{
    water_pressure_sensor_process();

#if (DS_MPS20N0040D_LOGGER_USE == 1)
    if (mps20n0040d_logging_cycles_get() >= DS_MPS20N0040D_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
        logger_dgb_print("[collector][mps20n0040d] water pressure depth = 0x%x cm\n", water_pressure_get_depth_cm());
        mps20n0040d_logging_cycles_reset();
    }
#endif
}
#endif /* DS_SYSTEM_DEPTH_DETECTOR_USE_MPS20N0040D */



static void sensors_data_collector_thread(void *params)
{
    (void)params;

    for(;;) {
        vTaskDelay(THREAD_OS_DELAY_MS / portTICK_PERIOD_MS);

        /* gather data from MPS20N0040D pressure sensor */
#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_MPS20N0040D)
        process_water_pressure_mps20n0040d_data();
# if (DS_MPS20N0040D_LOGGER_USE == 1)
        mps20n0040d_logging_cycles_inc();
# endif
#endif

#if defined(DS_SYSTEM_PISTON_DETECTOR_USE_VL53L0X)
        process_laser_sensor_vl53l0x_data();
        vl53l0_getting_data_cycles_inc();
# if (DS_VL53L0X_LOGGER_USE == 1)
        vl53l0_logging_cycles_inc();
# endif
#endif

#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS)
        process_laser_depth_sensor_tfminis_data();
# if (DS_TFMINIS_LOGGER_USE == 1)
        tfminis_logging_cycles_inc();
# endif
#endif

    }
}

void sensors_data_collector_thread_create(void)
{
    TaskHandle_t h;
    static StaticTask_t collector_task_tcb;
    static StackType_t collector_task_stack[configMINIMAL_STACK_SIZE];

    h = xTaskCreateStatic(sensors_data_collector_thread,
                          "SensorCollector",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          tskIDLE_PRIORITY + 1,
                          collector_task_stack,
                          &collector_task_tcb);
    SUB_ASSERT(h == NULL, SUB_SENSORS_DATA_COLLECTOR_THREAD_ASSERT_500, SUB_ASSERT_CRITICAL);
    logger_dgb_print("[collector] thread created!\n");
}
