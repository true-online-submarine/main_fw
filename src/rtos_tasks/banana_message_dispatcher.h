#ifndef __BANANA_MESSAGE_DISPATCHER_THREAD_H
#define __BANANA_MESSAGE_DISPATCHER_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void banana_message_dispatcher_thread_create(void);
void banana_message_dispatcher_handle_byte_isr(void);

#ifdef __cplusplus
}
#endif

#endif /* __BANANA_MESSAGE_DISPATCHER_THREAD_H */

