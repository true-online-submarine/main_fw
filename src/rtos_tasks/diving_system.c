#include <stdlib.h>
#include <stdint.h>

#include "diving_surfacing_system_config.h"
#include "diving_system.h"
#include "ds_system_api.h"
#include "ds_microswitch.h"
#include "main.h"

#include "FreeRTOS.h"
#include "task.h"

#include "sub_logger.h"
#include "sub_errors.h"

#include "ds_servo.h"
#include "light_api.h"

#define THREAD_OS_DELAY_MS (5U)

static ds_commands_t operation = 0;
static int new_op_flag = 0;

#if (DS_SYSTEM_USE_LOGGER == 1)
static int ds_operation_logging_cycles_cnt = 0;
static void ds_operation_logging_cycles_inc(void) { ds_operation_logging_cycles_cnt++; }
static void ds_operation_logging_cycles_reset(void) { ds_operation_logging_cycles_cnt = 0; }
static int ds_operation_logging_cycles_get(void) { return ds_operation_logging_cycles_cnt; }
#endif /* DS_SYSTEM_USE_LOGGER */

#if defined(DS_SYSTEM_USE_EMERGENCY_SURFACING)
static int emergency_flag = 0;
static int ds_emergency_surfacing_cycles_cnt = 0;
static void ds_emergency_surfacing_cycles_inc(void) { ds_emergency_surfacing_cycles_cnt++; }
static void ds_emergency_surfacing_cycles_reset(void) { ds_emergency_surfacing_cycles_cnt = 0; }
static int ds_emergency_surfacing_cycles_get(void) { return ds_emergency_surfacing_cycles_cnt; }

# if (DS_LED_BLINK_IF_EMERGENCY_SURFACING_DETECTED == 1)
static int ds_emergency_blink_cycles_cnt = 0;
static void ds_emergency_blink_cycles_inc(void) { ds_emergency_blink_cycles_cnt++; }
static void ds_emergency_blink_cycles_reset(void) { ds_emergency_blink_cycles_cnt = 0; }
static int ds_emergency_blink_cycles_get(void) { return ds_emergency_blink_cycles_cnt; }
# endif

#endif /* DS_SYSTEM_USE_EMERGENCY_SURFACING */

static ds_state_t handle_diving_cmd(void)
{
    ds_state_t ds_state = ds_go_down();

    switch (ds_state) {
    case DS_DIVING_OP_EXECUTION:
#if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] diving in progress..\n");
            ds_operation_logging_cycles_reset();
        }
#endif
        break;
    default:
        break;
    }

    return ds_state;
}

static ds_state_t handle_surfacing_cmd(void)
{
    ds_state_t ds_state = ds_go_up();

    switch (ds_state) {
    case DS_SURFACING_OP_EXECUTION:
#if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] surfacing in progress..\n");
            ds_operation_logging_cycles_reset();
        }
#endif
        break;
    default:
        break;
    }

#if (DS_LED_BLINK_IF_EMERGENCY_SURFACING_DETECTED == 1)
        if (ds_state == DS_ON_SURFACE) {
            light_brightness_min();
            emergency_flag = 0;
        }

        if (emergency_flag) {
            static int led_state = 1;
            if (ds_emergency_blink_cycles_get() >= DS_LED_BLINK_IF_EMERGENCY_SURFACING_PERIOD_MS / THREAD_OS_DELAY_MS) {
                ds_emergency_blink_cycles_reset();
                if (led_state) {
                    light_brightness_max();
                    led_state = 0;
                } else {
                    light_brightness_min();
                    led_state = 1;
                }
            }
        }
#endif

    return ds_state;
}

static void handle_up_down_cmd(void)
{
    ds_state_t state;

    state = ds_go_down();
#if (DS_SYSTEM_USE_LOGGER == 1)
    if (state == DS_DIVING_OP_EXECUTION && ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
        logger_dgb_print("[DS] diving in progress..\n");
        ds_operation_logging_cycles_reset();
    }
#endif
    if (state == DS_ON_BOTTOM) {
        HAL_Delay(2000);
    } 

    state = ds_go_up();
#if (DS_SYSTEM_USE_LOGGER == 1)
    if (state == DS_SURFACING_OP_EXECUTION && ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
        logger_dgb_print("[DS] surfacing in progress..\n");
        ds_operation_logging_cycles_reset();
    }
#endif
    if (state == DS_ON_SURFACE) {
        HAL_Delay(2000);
    }
}

#if (DS_DEBUG_USE_UNSAFE_COMMANDS == 1)
static void handle_unsafe_servo_cw_cmd(void)
{
    ds_servo_rotate_cw(DS_SERVO_UNSAFE_SERVO_CW_SPEED);
# if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] UNSAFE CW command in progress..\n");
            ds_operation_logging_cycles_reset();
        }
# endif
}

static void handle_unsafe_servo_ccw_cmd(void)
{
    ds_servo_rotate_ccw(DS_SERVO_UNSAFE_SERVO_CCW_SPEED);
# if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] UNSAFE CCW command in progress..\n");
            ds_operation_logging_cycles_reset();
        }
# endif
}

static ds_state_t handle_go_to_diving_switch_cmd(void)
{
#if (DS_DIVING_MICROSWITCH_USE == 1)
    #define MAX_TRIG_CNT 5
    static int trigger_cnt = 0;

    ds_servo_rotate_cw(DS_SERVO_UNSAFE_SERVO_CW_SPEED);
    if (ds_is_diving_switch_triggered()) {
        ++trigger_cnt;

        if (trigger_cnt == MAX_TRIG_CNT) {
            ds_stop_any_operations();
            trigger_cnt = 0;
            return DS_ON_BOTTOM;
        }
    } else {
        trigger_cnt = 0;
    }

# if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] go to diving microswitch in progress..\n");
            ds_operation_logging_cycles_reset();
        }
# endif
    return DS_DIVING_OP_EXECUTION;
#else
    logger_dgb_print("[DS] Diving switch isn't supported\n");
    return DS_ON_BOTTOM;
#endif
}

static ds_state_t handle_go_to_surfacing_switch_cmd(void)
{
#if (DS_SURFACING_MICROSWITCH_USE == 1)
    #define MAX_TRIG_CNT 5
    static int trigger_cnt = 0;

    ds_servo_rotate_ccw(DS_SERVO_UNSAFE_SERVO_CCW_SPEED);
    if (ds_is_surfacing_switch_triggered()) {
        ++trigger_cnt;

        if (trigger_cnt == MAX_TRIG_CNT) {
            ds_stop_any_operations();
            trigger_cnt = 0;
            return DS_ON_SURFACE;
        }
    } else {
        trigger_cnt = 0;
    }

# if (DS_SYSTEM_USE_LOGGER == 1)
        if (ds_operation_logging_cycles_get() >= DS_SYSTEM_LOGGING_PERIOD_MS / THREAD_OS_DELAY_MS) {
            logger_dgb_print("[DS] go to surfacing microswitch in progress..\n");
            ds_operation_logging_cycles_reset();
        }
# endif
    return DS_SURFACING_OP_EXECUTION;
#else
    logger_dgb_print("[DS] Surfacing switch isn't supported\n");
    return DS_ON_SURFACE;
#endif
}

#endif /* DS_DEBUG_USE_UNSAFE_COMMANDS */

static void ds_system_thread(void *params)
{
    (void)params;

    for(;;) {
        vTaskDelay(THREAD_OS_DELAY_MS / portTICK_PERIOD_MS);

        if (new_op_flag) {
            switch (operation) {
            case DS_CMD_DIVING:
                if (handle_diving_cmd() == DS_ON_BOTTOM) {
                    logger_dgb_print("[DS] diving done\n");
                    new_op_flag = 0;
                }
                break;
            case DS_CMD_SURFACING:
                if (handle_surfacing_cmd() == DS_ON_SURFACE) {
                    logger_dgb_print("[DS] surfacing done\n");
                    new_op_flag = 0;
                }
                break;
            case DS_CMD_UP_DOWN:
                handle_up_down_cmd();
                break;
#if (DS_DEBUG_USE_UNSAFE_COMMANDS == 1)
            case DS_UNSAFE_CMD_SERVO_GO_CW:
                handle_unsafe_servo_cw_cmd();
                break;
            case DS_UNSAFE_CMD_SERVO_GO_CCW:
                handle_unsafe_servo_ccw_cmd();
                break;
#endif
            case DS_CMD_SERVO_GO_TO_DIVING_SWITCH:
                if (handle_go_to_diving_switch_cmd() == DS_ON_BOTTOM) {
                    logger_dgb_print("[DS] go to diving switch done\n");
                    new_op_flag = 0;
                }
                break;
            case DS_CMD_SERVO_GO_TO_SURFACING_SWITCH:
                if (handle_go_to_surfacing_switch_cmd() == DS_ON_SURFACE) {
                    logger_dgb_print("[DS] go to surfacing switch done\n");
                    new_op_flag = 0;
                }
                break;
            default:
                break;
            }

            /* HIGH PRIORITY OPERATION */
            if (operation == DS_CMD_STOP_ALL) {
                ds_stop_any_operations();
                logger_dgb_print("[DS] stop\n");
                new_op_flag = 0;
            }
#if defined(DS_SYSTEM_USE_EMERGENCY_SURFACING)
            ds_emergency_surfacing_cycles_reset();
#endif
        }

#if defined(DS_SYSTEM_USE_EMERGENCY_SURFACING)
        if (ds_emergency_surfacing_cycles_get() >= (DS_EMERGENCY_SURFACING_AFTER_SECONDS * 1000) / THREAD_OS_DELAY_MS) {
            if ((ds_get_state() != DS_ON_SURFACE) && (emergency_flag == 0)) {
                emergency_flag = 1;
                ds_system_start_execute_cmd(DS_CMD_SURFACING);
                logger_dgb_print("[DS] ENERGENCY SURFACING EVENT DETECTED\n");
            }
        }
        ds_emergency_surfacing_cycles_inc();

# if (DS_LED_BLINK_IF_EMERGENCY_SURFACING_DETECTED == 1)
        ds_emergency_blink_cycles_inc();
# endif
#endif /* DS_SYSTEM_USE_EMERGENCY_SURFACING */

#if (DS_SYSTEM_USE_LOGGER == 1)
        ds_operation_logging_cycles_inc();
#endif
    }
}

void ds_system_start_execute_cmd(ds_commands_t ds_cmd)
{
    __disable_irq();
    operation = ds_cmd;
    new_op_flag = 1;
    __enable_irq();
}

void ds_system_thread_create(void)
{
    TaskHandle_t h;
    static StaticTask_t ds_task_tcb;
    static StackType_t ds_task_stack[configMINIMAL_STACK_SIZE];

    h = xTaskCreateStatic(ds_system_thread,
                          "DS",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          tskIDLE_PRIORITY + 1,
                          ds_task_stack,
                          &ds_task_tcb);
    SUB_ASSERT(h == NULL, SUB_DS_THREAD_ASSERT_505, SUB_ASSERT_CRITICAL);
    logger_dgb_print("[DS] thread created!\n");
}
