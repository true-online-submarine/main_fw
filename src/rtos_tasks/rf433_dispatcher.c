#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "pt2262_rx_info.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "sub_logger.h"
#include "sub_errors.h"

#include "light_api.h"
#include "ds_system_api.h"
#include "thruster_api.h"
#include "ds_servo.h"
#include "diving_system.h"

extern TIM_HandleTypeDef htim3;

/* RF message queue */
#define QUEUE_LENGTH 128
#define ITEM_SIZE sizeof(pt2262_half_bit_edges_t)

static int rf_queue_full_flag = 0;
static QueueHandle_t rf_queue_handle;
static uint8_t queue_storage_area[QUEUE_LENGTH * ITEM_SIZE];

#define RF433_NEUTRAL_CMD 0x0
#define RF433_THRUSTER_LEFT_UP_STEP 0x1
#define RF433_THRUSTER_LEFT_DOWN_STEP 0x2
#define RF433_DS_GO_DOWN 0x3
#define RF433_DS_GO_UP 0x4
#define RF433_THRUSTER_RIGHT_DOWN_STEP 0x5
#define RF433_THRUSTER_RIGHT_UP_STEP 0x6

static void handle_thruster_cmd(thruster_position_t pos, uint8_t cmd)
{
    int8_t steps = 0;
    uint8_t speed = thrusters_get_speed(pos);
    thruster_dir_t dir = thrusters_get_direction(pos);

    if (dir == THRUSTER_ROTATION_CW) {
        steps = speed;
    } else if (dir == THRUSTER_ROTATION_CCW) {
        steps = (-1) * speed;
    } else {
        steps = 0;
    }

    if ((speed == 0) && ((cmd == RF433_THRUSTER_LEFT_UP_STEP) || (cmd == RF433_THRUSTER_RIGHT_UP_STEP))) {
        dir = THRUSTER_ROTATION_CW;
        steps += ESC_THRUSTERS_POWER_STEP_PERCENTS;
    } else if ((speed == 0) && ((cmd == RF433_THRUSTER_LEFT_DOWN_STEP) || (cmd == RF433_THRUSTER_RIGHT_DOWN_STEP))) {
        dir = THRUSTER_ROTATION_CCW;
        steps -= ESC_THRUSTERS_POWER_STEP_PERCENTS;
    } else {
        if ((cmd == RF433_THRUSTER_LEFT_UP_STEP) || (cmd == RF433_THRUSTER_RIGHT_UP_STEP)) {
            if (steps < 100){
                steps += ESC_THRUSTERS_POWER_STEP_PERCENTS;
            }
        } else if ((cmd == RF433_THRUSTER_LEFT_DOWN_STEP) || (cmd == RF433_THRUSTER_RIGHT_DOWN_STEP)) {
            if (steps > -100) {
                steps -= ESC_THRUSTERS_POWER_STEP_PERCENTS;
            }
        }
    }

    if (dir == THRUSTER_ROTATION_CW) {
        speed = steps;
    } else if (dir == THRUSTER_ROTATION_CCW) {
        speed = (-1) * steps;
    } else {
        speed = 0;
    }

    thrusters_set_speed(pos, dir, speed);
}

static void handle_neutral_cmd(uint8_t prev_cmd)
{
    switch (prev_cmd) {
    case RF433_THRUSTER_LEFT_UP_STEP:
    case RF433_THRUSTER_LEFT_DOWN_STEP:
    case RF433_THRUSTER_RIGHT_DOWN_STEP:
    case RF433_THRUSTER_RIGHT_UP_STEP:
        break;
    case RF433_DS_GO_DOWN:
    case RF433_DS_GO_UP:
        ds_system_start_execute_cmd(DS_CMD_STOP_ALL);
        break;
    default:
        break;
    }
}

static void rf433_cmd_handler(uint8_t cmd)
{
    static uint8_t last_cmd = 0;

#if 1
    if (cmd != RF433_NEUTRAL_CMD) {
        logger_dgb_print("[RF433] rx cmd 0x%x\n", cmd);
    }
#endif

    switch (cmd) {
    case RF433_THRUSTER_LEFT_UP_STEP:
    case RF433_THRUSTER_LEFT_DOWN_STEP:
        handle_thruster_cmd(LEFT_MIDDLE_THRUSTER, cmd);
        break;
    case RF433_DS_GO_DOWN:
        ds_system_start_execute_cmd(DS_CMD_DIVING);
        break;
    case RF433_DS_GO_UP:
        ds_system_start_execute_cmd(DS_CMD_SURFACING);
        break;
    case RF433_THRUSTER_RIGHT_DOWN_STEP:
    case RF433_THRUSTER_RIGHT_UP_STEP:
        handle_thruster_cmd(RIGH_MIDDLE_THRUSTER, cmd);
        break;

    default:
        handle_neutral_cmd(last_cmd);
        break;
    }

    last_cmd = cmd;
}

static void rf433_message_dispatcher(void *params)
{
    (void)params;

    pt2262_half_bit_edges_t item = { 0 };
    uint8_t ret_cmd = 0;

    for (;;) {
        if (rf_queue_full_flag) {
            rf_queue_full_flag = 0;
            logger_dgb_print("[RF433] queue full\n");
        }

        if (xQueueReceive(rf_queue_handle, (void *)&item, portMAX_DELAY) == pdTRUE) {
            if (pt2262_rx_data_parser(&item, &ret_cmd) == RF_CMD_RECEIVED) {
                rf433_cmd_handler(ret_cmd);
            }
#if 0
            logger_dgb_print("%d %d %d %d\n", item.edge_h1, item.edge_l1, item.edge_h2, item.edge_l2);
#endif
        }
    }
}

void rf433_dispatcher_thread_create(void)
{
    TaskHandle_t h;
    static StaticTask_t rf433_task_tcb;
    static StackType_t rf433_task_stack[configMINIMAL_STACK_SIZE];

    h = xTaskCreateStatic(rf433_message_dispatcher,
                          "RF433-Dispatcher",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          tskIDLE_PRIORITY + 1,
                          rf433_task_stack,
                          &rf433_task_tcb);
    SUB_ASSERT(h == NULL, SUB_RF433_DISPATCHER_THREAD_ASSERT_515, SUB_ASSERT_CRITICAL);

    static StaticQueue_t rf_msg_static_queue;
    rf_queue_handle = xQueueCreateStatic(QUEUE_LENGTH,
                                         ITEM_SIZE,
                                         queue_storage_area,
                                         &rf_msg_static_queue);
    SUB_ASSERT(rf_queue_handle == NULL, SUB_RF433_DISPATCHER_THREAD_ASSERT_516, SUB_ASSERT_CRITICAL);

    if (HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_4) != HAL_OK) {
        SUB_ASSERT(1, SUB_RF433_DISPATCHER_THREAD_ASSERT_517, SUB_ASSERT_CRITICAL);
    }

    logger_dgb_print("[RF433] thread created!\n");
}

void rf433_dispatcher_edge_ic_isr_handler(void)
{
    /*
        How to detect on which edge we got interrupt?
        http://efton.sk/STM32/gotcha/g37.html

        As long as a pin is not set as Analog in GPIO_MODER,
        it is permanently connected to the input Schmitt trigger, and from that to GPIO_IDR, as well as to the EXTI input multiplexer.
        
        That means, that a pin's input state can be read, even if it is set as Out or AF1.
        This is useful especially when the output is set as Open Drain - in that case, level set in ODR for that
        pin does not necessarily match the level read in into IDR for the same pin.
        
        Similarly, EXTI can be used, even if pin is set as Out or AF.
        This allows for example to have an external interrupt together with pin acting as
        the dedicated alternate-function input for some module. One useful application for this is the NSS pin in SPI set as slave,
        used with DMA - the AF acts as framing/output enable for the hardware, and the rising edge could trigger an interrupt
        which in software starts the processing of received data. 
    */

    /* 
     * So just read the current pin state to clarify which duration we got.
     *  - Low level means that falling edge has been detected and we got duration of '1' level.  
     *  - High level means that rising edge has been detected and we got duration of '0' level.
     **/

    static int is_get_levels = 0;
    static pt2262_half_bit_edges_t item = { 0 };

    if ((RF433_RECEIVER_GPIO_Port->IDR & RF433_RECEIVER_Pin) == GPIO_PIN_RESET) { /* HIGH LEVEL DETECT */
        item.edge_h = TIM3->CCR4;
    } else { /* LOW LEVEL DETECT */
        item.edge_l = TIM3->CCR4;
        is_get_levels = 1;
    }

    if (is_get_levels) {
        is_get_levels = 0;

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xQueueSendFromISR(rf_queue_handle, (void *)&item, &xHigherPriorityTaskWoken) == errQUEUE_FULL) {
            rf_queue_full_flag = 1;
        }
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }

    /* Reset count register to get next value exact in us and avoid type overflow */
    TIM3->CNT = 0;
}
