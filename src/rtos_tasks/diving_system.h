#ifndef __DIVING_SYSTEM_H
#define __DIVING_SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum {
    DS_CMD_DIVING,
    DS_CMD_SURFACING,
    DS_CMD_UP_DOWN,
    DS_CMD_STOP_ALL,
    DS_CMD_SERVO_GO_TO_DIVING_SWITCH,
    DS_CMD_SERVO_GO_TO_SURFACING_SWITCH,
#if (DS_DEBUG_USE_UNSAFE_COMMANDS == 1)
    DS_UNSAFE_CMD_SERVO_GO_CW,
    DS_UNSAFE_CMD_SERVO_GO_CCW,
#endif
} ds_commands_t;

void ds_system_thread_create(void);
void ds_system_start_execute_cmd(ds_commands_t ds_cmd);

#ifdef __cplusplus
}
#endif

#endif /* __DIVING_SYSTEM_H */

