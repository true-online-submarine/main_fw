#ifndef __SENSORS_DATA_COLLECTOR_THREAD_H
#define __SENSORS_DATA_COLLECTOR_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void sensors_data_collector_thread_create(void);

#ifdef __cplusplus
}
#endif

#endif /* __SENSORS_DATA_COLLECTOR_THREAD_H */

