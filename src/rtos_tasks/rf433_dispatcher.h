#ifndef __RF433_DISPATCHER_THREAD_H
#define __RF433_DISPATCHER_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void rf433_dispatcher_thread_create(void);
void rf433_dispatcher_edge_ic_isr_handler(void);

#ifdef __cplusplus
}
#endif

#endif /* __RF433_DISPATCHER_THREAD_H */

