#include <stdlib.h>
#include <string.h>
#include "banana_message_dispatcher.h"
#include "main.h"

#include "FreeRTOS.h"
#include "task.h"

#include "sub_logger.h"
#include "sub_errors.h"

#include "banana_link.h"

#define THREAD_OS_DELAY_MS 10

extern UART_HandleTypeDef huart6;

typedef union {
    uint8_t rx_arr[sizeof(banana_message_t)];
    banana_message_t banana_message;
} rx_data_t;

static rx_data_t rx_data = { 0 };
static uint8_t rx_byte = 0;
static uint8_t rx_byte_idx = 0;
static uint8_t rx_byte_flag = 0;

static void banana_message_dispatcher(void *params)
{
    (void)params;

    for (;;) {
        vTaskDelay(THREAD_OS_DELAY_MS / portTICK_PERIOD_MS);

        if (rx_byte_flag) {
            banana_message_parse_execute(&rx_data.banana_message);
            rx_byte_flag = 0;
        }
    }
}

void banana_message_dispatcher_thread_create(void)
{
    TaskHandle_t h;
    static StaticTask_t banana_task_tcb;
    static StackType_t banana_task_stack[configMINIMAL_STACK_SIZE];

    h = xTaskCreateStatic(banana_message_dispatcher,
                          "BananaDispatcher",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          tskIDLE_PRIORITY + 1,
                          banana_task_stack,
                          &banana_task_tcb);
    SUB_ASSERT(h == NULL, SUB_BANANA_MESSAGE_DISPATCHER_THREAD_ASSERT_510, SUB_ASSERT_CRITICAL);

    if (HAL_UART_Receive_IT(&huart6, &rx_byte, 1) != HAL_OK) {
        SUB_ASSERT(1, SUB_BANANA_MESSAGE_DISPATCHER_THREAD_ASSERT_511, SUB_ASSERT_CRITICAL);
    }

    logger_dgb_print("[dispatcher] thread created!\n");
}

void banana_message_dispatcher_handle_byte_isr(void)
{
    if (rx_byte == '\n') {
        rx_byte_flag = 1;
        rx_byte_idx = 0;
    } else {
        if (rx_byte_idx >= sizeof(rx_data.rx_arr)) {
            rx_byte_idx = 0;
        }
        rx_data.rx_arr[rx_byte_idx++] = rx_byte;
    }

    HAL_UART_Receive_IT(&huart6, &rx_byte, 1);
}
