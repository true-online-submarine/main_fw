#include "non_os_delay.h"
#include "main.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "peripheral_init.h"

extern TIM_HandleTypeDef htim10;

#define NO_OS_DELAY_USE_IRQ 1
#define NO_OS_DELAY_DONT_USE_IRQ 0

void no_os_delay_timer_init_irq(void)
{
    tim10_counter_init(NO_OS_DELAY_USE_IRQ);
}

void no_os_delay_timer_disable_irq(void)
{
    tim10_counter_init(NO_OS_DELAY_DONT_USE_IRQ);
}

void no_os_delay_us(uint32_t us)
{
    TIM10->CNT = 0;
    while (TIM10->CNT < us);
}
