#ifndef __PERIPHERAL_INIT_H
#define __PERIPHERAL_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

void clock_init(void);
void gpio_init(void);
void i2c1_init(void);
void tim2_init(void);
void tim3_init(void);
void tim5_init(void);
void tim9_init(void);
void uart1_init(void);
void uart6_init(void);
void tim10_counter_init(int use_irq);
void systick_disable_int(void);
void dma2_stream2_init(void);

#ifdef __cplusplus
}
#endif

#endif /* __PERIPHERAL_INIT_H */
