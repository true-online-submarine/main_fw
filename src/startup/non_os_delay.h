#ifndef __NON_OS_DELAY_H
#define __NON_OS_DELAY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/*
    All this code is needed just to call HAL_IncTick() to make HAL_Delay() function working
    before the scheduler starts.
    
    Previously HAL_IncTick() was called by SysTick_Handler(). For now FreeRTOS uses the interrupt.
    And if the scheduler is in taskSCHEDULER_NOT_STARTED state, SysTick_Handler() trigger hardfault after 1ms elapsed.
    It happens, because the Freertos calls xTaskIncrementTick() in the interrupt. We get HF there during:
    xItemValue = listGET_LIST_ITEM_VALUE( &( pxTCB->xStateListItem ) );

    Might be we should open PR in FreeRTOS repository to add these lines to xPortSysTickHandler():
        if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED) {
            if( xTaskIncrementTick() != pdFALSE )
            {
                portNVIC_INT_CTRL_REG = portNVIC_PENDSVSET_BIT;
            }
        }

    Therefore for now setup other timer to work with delays.
    After OS starts we use vApplicationTickHook() function to call HAL_IncTick().

    Anyway no_os_delay_us() is available to make micro-second delays from 1us to 65535us
*/

void no_os_delay_timer_init_irq(void); /* call it in the beginning because SysTick handkler occupied by OS */
void no_os_delay_timer_disable_irq(void); /* call it before scheduler start */
void no_os_delay_us(uint32_t us); /* 1us - 65535us (~65.5ms) */

#ifdef __cplusplus
}
#endif

#endif /* __NON_OS_DELAY_H */
