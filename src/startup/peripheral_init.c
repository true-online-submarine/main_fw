#include "main.h"
#include "peripheral_init.h"
#include "sub_errors.h"

#include "FreeRTOSConfig.h"

I2C_HandleTypeDef hi2c1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim9;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart6;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim10;
DMA_HandleTypeDef hdma_usart1_rx;

void clock_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

    /** Configure the main internal regulator output voltage
     */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 4;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_CLOCK_ASSERT_0, SUB_ASSERT_CRITICAL);

    /** Initializes the CPU, AHB and APB buses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_CLOCK_ASSERT_1, SUB_ASSERT_CRITICAL);
}

void i2c1_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    hi2c1.Instance = I2C1;
    hi2c1.Init.ClockSpeed = 100000;
    hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    ret = HAL_I2C_Init(&hi2c1);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_I2C1_ASSERT_5, SUB_ASSERT_CRITICAL);
}

void tim2_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    TIM_MasterConfigTypeDef sMasterConfig = { 0 };
    TIM_OC_InitTypeDef sConfigOC = { 0 };

    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 83;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 20000;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_PWM_Init(&htim2);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM2_ASSERT_10, SUB_ASSERT_CRITICAL);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    ret = HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM2_ASSERT_11, SUB_ASSERT_CRITICAL);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

    ret = HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM2_ASSERT_12, SUB_ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM2_ASSERT_13, SUB_ASSERT_CRITICAL);

    HAL_TIM_MspPostInit(&htim2);
}

void tim5_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    TIM_MasterConfigTypeDef sMasterConfig = { 0 };
    TIM_OC_InitTypeDef sConfigOC = { 0 };

    htim5.Instance = TIM5;
    htim5.Init.Prescaler = 83;
    htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim5.Init.Period = 3030;
    htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_PWM_Init(&htim5);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM5_ASSERT_15, SUB_ASSERT_CRITICAL);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    ret = HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM5_ASSERT_16, SUB_ASSERT_CRITICAL);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    ret = HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_4);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM5_ASSERT_17, SUB_ASSERT_CRITICAL);

    HAL_TIM_MspPostInit(&htim5);
}

void tim9_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    TIM_OC_InitTypeDef sConfigOC = { 0 };

    htim9.Instance = TIM9;
    htim9.Init.Prescaler = 83;
    htim9.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim9.Init.Period = 500; // 2kHz
    htim9.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim9.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_PWM_Init(&htim9);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM9_ASSERT_20, SUB_ASSERT_CRITICAL);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    ret = HAL_TIM_PWM_ConfigChannel(&htim9, &sConfigOC, TIM_CHANNEL_1);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM9_ASSERT_21, SUB_ASSERT_CRITICAL);

    HAL_TIM_MspPostInit(&htim9);
}

void uart1_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_USART1_FORCE_RESET();
    __HAL_RCC_USART1_RELEASE_RESET();

    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    ret = HAL_UART_Init(&huart1);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_UART1_ASSERT_25, SUB_ASSERT_CRITICAL);
}

void uart6_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_USART6_CLK_ENABLE();
    __HAL_RCC_USART6_FORCE_RESET();
    __HAL_RCC_USART6_RELEASE_RESET();

    huart6.Instance = USART6;
    huart6.Init.BaudRate = 115200;
    huart6.Init.WordLength = UART_WORDLENGTH_8B;
    huart6.Init.StopBits = UART_STOPBITS_1;
    huart6.Init.Parity = UART_PARITY_NONE;
    huart6.Init.Mode = UART_MODE_TX_RX;
    huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart6.Init.OverSampling = UART_OVERSAMPLING_16;
    ret = HAL_UART_Init(&huart6);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_UART6_ASSERT_30, SUB_ASSERT_CRITICAL);
}

void gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, PRESSURE_SENS_SCK_Pin|VL53L0X_XSHT_Pin|EXT_TEMP_1W_Pin|INSIDE_TEMP_1W_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin : ONBOARD_LED_Pin */
    GPIO_InitStruct.Pin = ONBOARD_LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ONBOARD_LED_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : PRESSURE_SENS_OUT_Pin */
    GPIO_InitStruct.Pin = PRESSURE_SENS_OUT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(PRESSURE_SENS_OUT_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : PRESSURE_SENS_SCK_Pin */
    GPIO_InitStruct.Pin = PRESSURE_SENS_SCK_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PRESSURE_SENS_SCK_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : VL53L0X_XSHT_Pin */
    GPIO_InitStruct.Pin = VL53L0X_XSHT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(VL53L0X_XSHT_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : VL53L0X_INT_Pin */
    GPIO_InitStruct.Pin = VL53L0X_INT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(VL53L0X_INT_GPIO_Port, &GPIO_InitStruct);

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

    /*Configure GPIO pins : EXT_TEMP_1W_Pin INSIDE_TEMP_1W_Pin */
    GPIO_InitStruct.Pin = EXT_TEMP_1W_Pin|INSIDE_TEMP_1W_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pin : DIVING_SWITCH_Pin */
    GPIO_InitStruct.Pin = DIVING_SWITCH_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(DIVING_SWITCH_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : SURFACING_SWITCH_Pin */
    GPIO_InitStruct.Pin = SURFACING_SWITCH_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(SURFACING_SWITCH_Port, &GPIO_InitStruct);

    /*Configure GPIO pin : RF433_RECEIVER_Pin */
    GPIO_InitStruct.Pin = RF433_RECEIVER_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(RF433_RECEIVER_GPIO_Port, &GPIO_InitStruct);
}

void tim10_counter_init(int use_irq)
{
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_TIM10_CLK_ENABLE();

    /* disable timer */
    TIM10->DIER &= ~(TIM_IT_UPDATE);
    TIM10->CR1 &= ~(TIM_CR1_CEN);

    htim10.Instance = TIM10;
    htim10.Init.Prescaler = 84 - 1;
    htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim10.Init.Period = use_irq ? 1000 : 65535; // 1 ms interrupt or just max counter to do less 1ms delays
    htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_Base_Init(&htim10);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM10_ASSERT_45, SUB_ASSERT_CRITICAL);

    if (use_irq) {
        /* This timer is used to generate 1ms for HAL_Delay().
        For this we need the interrupt enabled. Therefore TIM1_UP_TIM10_IRQn priority must be more
        than masked by Freertos.
        
        (FreeRTOS maSkes interrupts lower then
        configMAX_SYSCALL_INTERRUPT_PRIORITY before the scheduler starts)!!
        Actually RTOS don't UNMASK previously masked IRQ.

        For example we called xTaskCreateStatic() first time.
        uxCriticalNesting variable hasn't been initialized yet. It is done by the xPortStartScheduler().
        uxCriticalNesting is static variable which value is 0xaaaaaaaa.
        Therefore function vPortExitCritical() believes that we have nested calls for entering
        ctirical sectoin. Might be it is a BUG. Might be not. Anyway scheduler initialize the
        variable during start. But for know we must remember that if we want use interrupts
        before OS start (f.i. to implement increment for HAL_Delay function) we MUST
        use priority larger than interrupt's priority masked by OS.

         ! For Cortex-M3 the lower number the larger proirity !
        */

        HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY >> __NVIC_PRIO_BITS) - 1, 0);
        HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);

        ret = HAL_TIM_Base_Start_IT(&htim10);
    } else {
        HAL_NVIC_DisableIRQ(TIM1_UP_TIM10_IRQn);

        ret = HAL_TIM_Base_Start(&htim10);
    }

    SUB_ASSERT(ret != HAL_OK, SUB_INIT_TIM10_ASSERT_46, SUB_ASSERT_CRITICAL);
}

void systick_disable_int(void)
{
    SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk| SysTick_CTRL_ENABLE_Msk);
}

void dma2_stream2_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    /* DMA controller clock enable */
    __HAL_RCC_DMA2_CLK_ENABLE();

    /* USART1 DMA Init */
    /* USART1_RX Init */
    hdma_usart1_rx.Instance = DMA2_Stream2;
    hdma_usart1_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart1_rx.Init.Mode = DMA_NORMAL;
    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_LOW;
    hdma_usart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    ret = HAL_DMA_Init(&hdma_usart1_rx);
    SUB_ASSERT(ret != HAL_OK, SUB_INIT_DMA2_STREAM2_ASSERT_40, SUB_ASSERT_CRITICAL);

    __HAL_LINKDMA(&huart1, hdmarx, hdma_usart1_rx);

    /* DMA interrupt init */
    /* DMA2_Stream2_IRQn interrupt configuration */
    HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
}

void tim3_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
    TIM_MasterConfigTypeDef sMasterConfig = { 0 };
    TIM_IC_InitTypeDef sConfigIC = { 0 };

    __HAL_RCC_TIM3_CLK_ENABLE();

    /* disable timer */
    TIM3->DIER &= ~(TIM_IT_CC4);
    TIM3->CR1 &= ~(TIM_CR1_CEN);

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 84 - 1; // 1 000 000 Hz
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 65535; // 1 tick = 1 us
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1; // fDTS = 1 000 000
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_IC_Init(&htim3);
    SUB_ASSERT(ret != HAL_OK, SUB_RF433_RX470C_V01_ASSERT_35, SUB_ASSERT_CRITICAL);

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    ret = HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);
    SUB_ASSERT(ret != HAL_OK, SUB_RF433_RX470C_V01_ASSERT_36, SUB_ASSERT_CRITICAL);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    ret = HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
    SUB_ASSERT(ret != HAL_OK, SUB_RF433_RX470C_V01_ASSERT_37, SUB_ASSERT_CRITICAL);

    /* Read the AN4776 for more information about filtering
       And also:
       - https://community.st.com/t5/stm32-mcus-motor-control/how-to-choose-the-timer-input-capture-filter-value/td-p/144889
       - http://efton.sk/STM32/gotcha/g78.html

        78.Using TIM input channel filter may produce confusing results (if fed with periodic signal)
        The TIM channels at their inputs incorporate a digital filter. This helps to filter out glitches and other unwanted signals, especially if the signal source is "real world" rather than other digital circuitry. The input signal is then used either to Capture the timer's counter current state, or as an input to the Slave-Mode controller.
        The filter samples the input signal continuously, and if the input level is about to change, only if it sees N samples of this new level consecutively, the filter outputs this new level, otherwise its output does not change. N and the sampling frequency can be set for each channel separately in TIMx_CCMRx.ICxF.
        N ranges from 2 to 8, and the sampling frequency is either equal to the internal clock of the timer, or is a fraction (1/2 to 1/32) of output of a prescaler called DTS (dead-time and sampling clock), derived from the same internal clock of timer, being equal to that clock or divided to 1/2 or 1/4, according to TIMx_CR1.CKD.
        This works quite well for spurious oscillations coming from bouncing mechanical contacts of switches. However, if the input signal changes periodically, e.g. due to electromagnetic interference from a neighbouring circuit into a high-impedance circuit of a disconnected switch, the results might be strange to confusing.
        If the input frequency is an exact multiple of the sampling frequency (which may easily happen if the interfering signal is derived from the same mcu clock than the timer's clock), the filter's output may be a steady level. For example, if the closed switch results in low input, but after opening the switch the interference would cause the filter sample low at every its sampling period, the net output is the same as if the switch would never open.
        An even more confusing result stems from an input signal which period is slightly different from exact multiple of the sampling frequency. In such a case, the output of filter would change at a low rate, corresponding to the beat frequency between sampling and input signal, possibly misleading the program/observer to believe that the input signal is of a much lower frequency than it really is.
        The corollary is, that while the input filter on timer channels may be a good tool to filter out spurious oscillations at signal's edges; where periodic input signal is expected, an external analog low-pass filter has to be included into the signal path. 
    */
    sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_BOTHEDGE;
    sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
    sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;

    /*
        0000: No filter, sampling is done at fDTS
        0001: fSAMPLING=fCK_INT, N=2
        0010: fSAMPLING=fCK_INT, N=4
        0011: fSAMPLING=fCK_INT, N=8
        0100: fSAMPLING=fDTS/2, N=6
        0101: fSAMPLING=fDTS/2, N=8
        0110: fSAMPLING=fDTS/4, N=6
        0111: fSAMPLING=fDTS/4, N=8
        1000: fSAMPLING=fDTS/8, N=6
        1001: fSAMPLING=fDTS/8, N=8
        1010: fSAMPLING=fDTS/16, N=5
        1011: fSAMPLING=fDTS/16, N=6
        1100: fSAMPLING=fDTS/16, N=8
        1101: fSAMPLING=fDTS/32, N=5
        1110: fSAMPLING=fDTS/32, N=6
        1111: fSAMPLING=fDTS/32, N=8    
     */
    sConfigIC.ICFilter = 3;
    ret = HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_4);
    SUB_ASSERT(ret != HAL_OK, SUB_RF433_RX470C_V01_ASSERT_38, SUB_ASSERT_CRITICAL);

    /* set the priority to maskable by FreeRTOS due to we use FronISR API */
    HAL_NVIC_SetPriority(TIM3_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY >> __NVIC_PRIO_BITS) + 1, 0);
    HAL_NVIC_EnableIRQ(TIM3_IRQn);
}
