#include "main.h"
#include "FreeRTOSConfig.h"

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

void HAL_MspInit(void)
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
}

void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    if(hi2c->Instance==I2C1) {
        __HAL_RCC_GPIOB_CLK_ENABLE();
        /**I2C1 GPIO Configuration
        PB6     ------> I2C1_SCL
        PB7     ------> I2C1_SDA
        */
        GPIO_InitStruct.Pin = VL53l0X_I2C1_SCL_Pin|VL53l0X_I2C1_SDA_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

        __HAL_RCC_I2C1_CLK_ENABLE();
    }
}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{
    if(hi2c->Instance==I2C1) {
        __HAL_RCC_I2C1_CLK_DISABLE();

        /**I2C1 GPIO Configuration
        PB6     ------> I2C1_SCL
        PB7     ------> I2C1_SDA
        */
        HAL_GPIO_DeInit(VL53l0X_I2C1_SCL_GPIO_Port, VL53l0X_I2C1_SCL_Pin);

        HAL_GPIO_DeInit(VL53l0X_I2C1_SDA_GPIO_Port, VL53l0X_I2C1_SDA_Pin);
    }
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
    if(htim_pwm->Instance==TIM2) {
        __HAL_RCC_TIM2_CLK_ENABLE();
    } else if(htim_pwm->Instance==TIM5) {
        __HAL_RCC_TIM5_CLK_ENABLE();
    } else if(htim_pwm->Instance==TIM9) {
        __HAL_RCC_TIM9_CLK_ENABLE();
    }
}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    if(htim->Instance==TIM2) {
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**TIM2 GPIO Configuration
        PA0-WKUP     ------> TIM2_CH1
        PA1     ------> TIM2_CH2
        */
        GPIO_InitStruct.Pin = LEFT_ENGINE_PWM_TIM2_CH1_Pin|RIGHT_ENGINE_PWM_TIM2_CH2_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    } else if(htim->Instance==TIM5) {
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**TIM5 GPIO Configuration
        PA3     ------> TIM5_CH4
        */
        GPIO_InitStruct.Pin = SERVO_PWM_TIM5_CH4_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
        HAL_GPIO_Init(SERVO_PWM_TIM5_CH4_GPIO_Port, &GPIO_InitStruct);
    } else if(htim->Instance==TIM9) {
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**TIM9 GPIO Configuration
        PA2     ------> TIM9_CH1
        */
        GPIO_InitStruct.Pin = LED_PWM_TIM9_CH1_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
        HAL_GPIO_Init(LED_PWM_TIM9_CH1_GPIO_Port, &GPIO_InitStruct);
    }
}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* htim_pwm)
{
    if(htim_pwm->Instance==TIM2) {
        __HAL_RCC_TIM2_CLK_DISABLE();
    }
    else if(htim_pwm->Instance==TIM5) {
        __HAL_RCC_TIM5_CLK_DISABLE();
    }
    else if(htim_pwm->Instance==TIM9) {
        __HAL_RCC_TIM9_CLK_DISABLE();
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    if(huart->Instance==USART1) {
        __HAL_RCC_USART1_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        GPIO_InitStruct.Pin = TF_MINI_S_USART1_TX_Pin|TF_MINI_S_USART1_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* USART1 interrupt Init */
        HAL_NVIC_EnableIRQ(USART1_IRQn);
    } else if(huart->Instance==USART6) {
        __HAL_RCC_USART6_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART6 GPIO Configuration
        PA11     ------> USART6_TX
        PA12     ------> USART6_RX
        */
        GPIO_InitStruct.Pin = LOG_USART6_TX_Pin|LOG_USART6_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
        GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* USART6 interrupt Init */
        HAL_NVIC_EnableIRQ(USART6_IRQn);
    }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
    if(huart->Instance==USART1) {
        __HAL_RCC_USART1_CLK_DISABLE();

        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        HAL_GPIO_DeInit(GPIOA, TF_MINI_S_USART1_TX_Pin|TF_MINI_S_USART1_RX_Pin);

        /* USART1 interrupt DeInit */
        HAL_NVIC_DisableIRQ(USART1_IRQn);
    } else if(huart->Instance==USART6) {
        /* Peripheral clock disable */
        __HAL_RCC_USART6_CLK_DISABLE();

        /**USART6 GPIO Configuration
        PA11     ------> USART6_TX
        PA12     ------> USART6_RX
        */
        HAL_GPIO_DeInit(GPIOA, LOG_USART6_TX_Pin|LOG_USART6_RX_Pin);

        /* USART6 interrupt DeInit */
        HAL_NVIC_DisableIRQ(USART6_IRQn);
    }
}
