#ifndef __ESC_ADAPTER_H
#define __ESC_ADAPTER_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(THRUSTERS_USE_BLDC_MOTORS)

#include <stdint.h>
#include "thrusters_config.h"

void esc_adapter_init(void);
void esc_adapter_left_setup(thruster_dir_t rot, uint8_t speed_percents);
void esc_adapter_right_setup(thruster_dir_t rot, uint8_t speed_percents);

#ifdef __cplusplus
}
#endif

#endif /* THRUSTERS_USE_BLDC_MOTORS */

#endif /* __ESC_ADAPTER_H */
