#include "esc_adapter.h"
#include "thrusters_config.h"
#include "sub_errors.h"
#include "main.h"

#if defined(THRUSTERS_USE_BLDC_MOTORS)

extern TIM_HandleTypeDef htim2;

void esc_adapter_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

#if defined(ESC_USE_JETI_MODEL_ADVANCED)
    TIM2->CCR1 = ESC_MAX_PWM_US; /* Left engine */
    TIM2->CCR2 = ESC_MAX_PWM_US; /* Right engine */

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
    SUB_ASSERT(ret != HAL_OK, SUB_THRUSTERS_ASSERT_110, SUB_ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
    SUB_ASSERT(ret != HAL_OK, SUB_THRUSTERS_ASSERT_111, SUB_ASSERT_CRITICAL);

    HAL_Delay(ESC_MIN_DELAY_BEFORE_START_MS);

    TIM2->CCR1 = ESC_MIN_PWM_US; /* Left engine */
    TIM2->CCR2 = ESC_MIN_PWM_US; /* Right engine */

    HAL_Delay(ESC_MIN_DELAY_BEFORE_START_MS);
#elif defined(ESC_USE_ZMR_UNKNOWN_BIDIR)
    TIM2->CCR1 = ESC_NEUTRAL_POINT_PWM_US; /* Left engine */
    TIM2->CCR2 = ESC_NEUTRAL_POINT_PWM_US; /* Right engine */

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
    SUB_ASSERT(ret != HAL_OK, SUB_THRUSTERS_ASSERT_110, SUB_ASSERT_CRITICAL);

    ret = HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
    SUB_ASSERT(ret != HAL_OK, SUB_THRUSTERS_ASSERT_111, SUB_ASSERT_CRITICAL);

    HAL_Delay(ESC_MIN_DELAY_BEFORE_START_MS);
#endif
}

void esc_adapter_left_setup(thruster_dir_t rot, uint8_t speed_percents)
{
#if (ESC_SW_BIDIRECTION_SUPPORT == 1)
    if (rot == THRUSTER_ROTATION_CW) {
        TIM2->CCR1 = ESC_NEUTRAL_POINT_PWM_US + (speed_percents * (ESC_MAX_PWM_US - ESC_NEUTRAL_POINT_PWM_US) / 100);
    } else if (rot == THRUSTER_ROTATION_CCW) {
        TIM2->CCR1 = ESC_NEUTRAL_POINT_PWM_US - (speed_percents * (ESC_NEUTRAL_POINT_PWM_US - ESC_MIN_PWM_US) / 100);
    } else {
        TIM2->CCR1 = ESC_NEUTRAL_POINT_PWM_US;
    }
#else
    TIM2->CCR1 = ESC_MIN_PWM_US + (speed_percents * (ESC_MAX_PWM_US - ESC_MIN_PWM_US) / 100);
#endif
}

void esc_adapter_right_setup(thruster_dir_t rot, uint8_t speed_percents)
{
#if (ESC_SW_BIDIRECTION_SUPPORT == 1)
    if (rot == THRUSTER_ROTATION_CW) {
        TIM2->CCR2 = ESC_NEUTRAL_POINT_PWM_US + (speed_percents * (ESC_MAX_PWM_US - ESC_NEUTRAL_POINT_PWM_US) / 100);
    } else if (rot == THRUSTER_ROTATION_CCW) {
        TIM2->CCR2 = ESC_NEUTRAL_POINT_PWM_US - (speed_percents * (ESC_NEUTRAL_POINT_PWM_US - ESC_MIN_PWM_US) / 100);
    } else {
        TIM2->CCR2 = ESC_NEUTRAL_POINT_PWM_US;
    }
#else
    TIM2->CCR2 = ESC_MIN_PWM_US + (speed_percents * (ESC_MAX_PWM_US - ESC_MIN_PWM_US) / 100);
#endif
}

#endif /* THRUSTERS_USE_BLDC_MOTORS */
