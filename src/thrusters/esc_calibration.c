#include "esc_calibration.h"
#include "main.h"

#if defined(THRUSTERS_USE_BLDC_MOTORS)
void thruster_esc_calibrate(void (*delay_ms)(uint32_t ms))
{
#if defined(ESC_USE_JETI_MODEL_ADVANCED)
    /*
        TODO:
        Read datasheet here https://true-online-sub.youtrack.cloud/issue/TOS-40
        (link in comments)

        The JETI has wide system of configaration and calibration.
        Read the doc and implement it.
    */
#endif /* ESC_USE_JETI_MODEL_ADVANCED */
}
#endif /* THRUSTERS_USE_BLDC_MOTORS */
