#ifndef __ESC_CALIBRATION_H
#define __ESC_CALIBRATION_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if defined(THRUSTERS_USE_BLDC_MOTORS)
void thruster_esc_calibrate(void (*delay_ms)(uint32_t ms));
#endif

#ifdef __cplusplus
}
#endif

#endif /* __ESC_CALIBRATION_H */
