#ifndef __THRUSTER_API_H
#define __THRUSTER_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "thrusters_config.h"
#include "esc_adapter.h"

typedef struct {
    thruster_position_t position_id;
    void (*set_speed)(thruster_dir_t rot, uint8_t speed); /* value in % from min to max. See thrusters_config.h */
    thruster_dir_t _cur_rotation;
    uint8_t _cur_speed;
} thruster_t;

void thrusters_init(void);
void thrusters_stop_all(void);

void thrusters_set_speed(thruster_position_t position_id, thruster_dir_t rot, uint8_t speed_percents);
uint8_t thrusters_get_speed(thruster_position_t position_id);
thruster_dir_t thrusters_get_direction(thruster_position_t position_id);

void thrusters_swim_forward(uint8_t speed_percents);
void thrusters_swim_backward(uint8_t speed_percents);

#ifdef __cplusplus
}
#endif

#endif /* __THRUSTER_API_H */
