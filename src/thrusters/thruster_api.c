#include "thruster_api.h"
#include "thrusters_config.h"
#include "esc_adapter.h"
#include "sub_logger.h"

static thruster_t thrusters[MAX_THRUSTER_COUNTS] = {
    [LEFT_MIDDLE_THRUSTER] = { LEFT_MIDDLE_THRUSTER, esc_adapter_left_setup },
    [RIGH_MIDDLE_THRUSTER] = { RIGH_MIDDLE_THRUSTER, esc_adapter_right_setup },
};

void thrusters_init(void)
{
#if defined(THRUSTERS_USE_BLDC_MOTORS)
    esc_adapter_init();
#endif

    thrusters_set_speed(LEFT_MIDDLE_THRUSTER, THRUSTER_ROTATION_NEUTRAL, 0);
    thrusters_set_speed(RIGH_MIDDLE_THRUSTER, THRUSTER_ROTATION_NEUTRAL, 0);

    logger_dgb_print("[thrusters] init done\n");
}

void thrusters_stop_all(void)
{
    for (int i = 0; i < sizeof(thrusters)/sizeof(thrusters[0]); ++i) {
        thrusters[i].set_speed(THRUSTER_ROTATION_NEUTRAL, 0);
        thrusters[i]._cur_rotation = THRUSTER_ROTATION_NEUTRAL;
        thrusters[i]._cur_speed = 0;
    }

    logger_dgb_print("[thrusters] stop all\n");
}

void thrusters_set_speed(thruster_position_t position_id, thruster_dir_t rot, uint8_t speed_percents)
{
    if (position_id > MAX_THRUSTER_COUNTS - 1) {
        return;
    }

    if (speed_percents > 100) {
        speed_percents = 100;
    }

    thrusters[position_id]._cur_rotation = rot;
    thrusters[position_id]._cur_speed = speed_percents;

    thrusters[position_id].set_speed(rot, speed_percents);

    logger_dgb_print("[thrusters (%s)] set power to %d%%, %s\n",
                     position_id == LEFT_MIDDLE_THRUSTER ? "left middle" : "right middle",
                     thrusters[position_id]._cur_speed,
                     thrusters[position_id]._cur_rotation == THRUSTER_ROTATION_CW ? "CW" : "CCW");
}

uint8_t thrusters_get_speed(thruster_position_t position_id)
{
    logger_dgb_print("[thruster (%s)] current power is %d%%, %s\n",
                     position_id == LEFT_MIDDLE_THRUSTER ? "left middle" : "right middle",
                     thrusters[position_id]._cur_speed,
                     thrusters[position_id]._cur_rotation == THRUSTER_ROTATION_CW ? "CW" : "CCW");

    return thrusters[position_id]._cur_speed;
}

thruster_dir_t thrusters_get_direction(thruster_position_t position_id)
{
    logger_dgb_print("[thruster (%s)] current rotation is %s\n",
                     position_id == LEFT_MIDDLE_THRUSTER ? "left middle" : "right middle",
                     thrusters[position_id]._cur_rotation == THRUSTER_ROTATION_CW ? "CW" : "CCW");

    return thrusters[position_id]._cur_rotation;
}

void thrusters_swim_forward(uint8_t speed_percents)
{
    for (int i = 0; i < sizeof(thrusters)/sizeof(thrusters[0]); ++i) {
        thrusters[i]._cur_rotation = THRUSTER_ROTATION_CW;
        thrusters[i]._cur_speed = speed_percents;
        thrusters[i].set_speed(THRUSTER_ROTATION_CW, speed_percents);
    }

    logger_dgb_print("[thrusters] All swim forward\n");
}

void thrusters_swim_backward(uint8_t speed_percents)
{
    for (int i = 0; i < sizeof(thrusters)/sizeof(thrusters[0]); ++i) {
        thrusters[i]._cur_rotation = THRUSTER_ROTATION_CCW;
        thrusters[i]._cur_speed = speed_percents;
        thrusters[i].set_speed(THRUSTER_ROTATION_CCW, speed_percents);
    }

    logger_dgb_print("[thrusters] All swim backward\n");
}
