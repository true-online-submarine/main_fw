#ifndef __LIGHT_API_H
#define __LIGHT_API_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void light_init(void);
void light_brightness_max(void);
void light_brightness_min(void);
void light_set_brightness(uint8_t percents);
uint8_t light_get_cur_brightness(void);

#ifdef __cplusplus
}
#endif

#endif /* __LIGHT_API_H */
