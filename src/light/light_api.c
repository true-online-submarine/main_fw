#include "light_api.h"
#include "main.h"
#include "sub_logger.h"
#include "sub_errors.h"

extern TIM_HandleTypeDef htim9;

void light_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    TIM9->CCR1 = 0;

    ret = HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
    SUB_ASSERT(ret != HAL_OK, SUB_LED_ASSERT_115, SUB_ASSERT_CRITICAL);

    logger_dgb_print("[light] init done\n");
}

void light_brightness_max(void)
{
    TIM9->CCR1 = 500;
    logger_dgb_print("[LED] brightness max\n");
}

void light_brightness_min(void)
{
    TIM9->CCR1 = 0;
    logger_dgb_print("[LED] brightness min\n");
}

void light_set_brightness(uint8_t percents)
{
    if (percents > 100) {
        percents = 100;
    }

    TIM9->CCR1 = (500 * percents) / 100;
    logger_dgb_print("[LED] brightness %d%%\n", percents);
}

uint8_t light_get_cur_brightness(void)
{
    uint8_t percents = 0;

    percents = (TIM9->CCR1 * 100) / 500;

    logger_dgb_print("[LED] get brightness = %d%%\n", percents);
    return percents;
}
