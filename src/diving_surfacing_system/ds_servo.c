#include "main.h"
#include "ds_servo.h"
#include "diving_surfacing_system_config.h"

#if defined(DS_SYSTEM_BASED_ON_SYRINGE)
extern TIM_HandleTypeDef htim5;

void ds_servo_init(void)
{
    HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);
    TIM5->CCR4 = DS_SERVO_NEUTRAL_POINT_PWM_US;
}

void ds_servo_rotate_cw(uint8_t speed_percent)
{
    if (speed_percent > 100) {
        speed_percent = 100;
    }

    TIM5->CCR4 = (DS_SERVO_NEUTRAL_POINT_PWM_US - (speed_percent * (DS_SERVO_NEUTRAL_POINT_PWM_US - DS_SERVO_MAX_CW_US)) / 100);
}

void ds_servo_rotate_ccw(uint8_t speed_percent)
{
    if (speed_percent > 100) {
        speed_percent = 100;
    }

    TIM5->CCR4 = (DS_SERVO_NEUTRAL_POINT_PWM_US + (speed_percent * (DS_SERVO_MAX_CCW_US - DS_SERVO_NEUTRAL_POINT_PWM_US)) / 100);
}

void ds_servo_neutral_point(void)
{
    /* neutral point */
    TIM5->CCR4 = DS_SERVO_NEUTRAL_POINT_PWM_US;
}

#endif /* DS_SYSTEM_BASED_ON_SYRINGE */
