#ifndef __DEPTH_LASER_SENSOR_ADAPTER_H
#define __DEPTH_LASER_SENSOR_ADAPTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

typedef struct {
    uint16_t distanse_cm;
    uint16_t err_reason; /* tfminis_dist_error_reason_t. See tf_mini_s.h */
} tof_sensor_data_t;

void depth_tof_lidar_sensor_init(void);
tof_sensor_data_t depth_tof_lidar_get_distance_cm(void);
uint16_t depth_tof_lidar_get_temp(void);
int depth_tof_lidar_is_new_data_available(void);

void depth_tof_lidar_uart_irq_handler(void);
void depth_tof_lidar_dma_irq_handler(size_t rx_dma_byte_cnt);

#ifdef __cplusplus
}
#endif

#endif /* __DEPTH_LASER_SENSOR_ADAPTER_H */
