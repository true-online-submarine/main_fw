#include "ds_microswitch.h"
#include "main.h"
#include "diving_surfacing_system_config.h"

#if defined(DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH)

#if (DS_DIVING_MICROSWITCH_USE == 1)
int ds_is_diving_switch_triggered(void)
{
    if (HAL_GPIO_ReadPin(DIVING_SWITCH_Port, DIVING_SWITCH_Pin) == GPIO_PIN_RESET) {
        return 1;
    }

    return 0;
}
#endif

#if (DS_SURFACING_MICROSWITCH_USE == 1)
int ds_is_surfacing_switch_triggered(void)
{
    if (HAL_GPIO_ReadPin(SURFACING_SWITCH_Port, SURFACING_SWITCH_Pin) == GPIO_PIN_RESET) {
        return 1;
    }

    return 0;
}
#endif

#endif /* DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH */
