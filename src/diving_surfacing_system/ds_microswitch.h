#ifndef __DS_MICROSWITCH_H
#define __DS_MICROSWITCH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "diving_surfacing_system_config.h"

#if defined(DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH)

# if (DS_DIVING_MICROSWITCH_USE == 1)
int ds_is_diving_switch_triggered(void);
# endif

# if (DS_SURFACING_MICROSWITCH_USE == 1)
int ds_is_surfacing_switch_triggered(void);
# endif

#endif /* DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH */

#ifdef __cplusplus
}
#endif

#endif /* __DS_MICROSWITCH_H */
