#ifndef __DS_SERVO_H
#define __DS_SERVO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void ds_servo_init(void);
void ds_servo_rotate_cw(uint8_t speed_percent);
void ds_servo_rotate_ccw(uint8_t speed_percent);
void ds_servo_neutral_point(void);

#ifdef __cplusplus
}
#endif

#endif /* __DS_SERVO_H */
