#include "ds_system_api.h"
#include "diving_surfacing_system_config.h"
#include "piston_range_sensor_adapter.h"
#include "depth_laser_sensor_adapter.h"
#include "hx710b_mps20n0040d_adapter.h"
#include "ds_servo.h"
#include "ds_microswitch.h"

#include <stdint.h>

#include "sub_logger.h"

#define CNT_SAME_RANGE_VALUES 20
#define SWITCH_MAX_COUNTER 5

static ds_state_t ds_state = DS_UNKNOWN;

void ds_init(void)
{
    ds_servo_init();
    ds_state = DS_UNKNOWN;

    /* Init VL53L0X laser range sensor to detect diving system piston position */
    piston_range_sensor_init();

    /* Init TFmini-s laser sensor to measure immersion depth */
    depth_tof_lidar_sensor_init();

    /* Init ADC hx710b to work with pressure sensor */
    water_pressure_sensor_adc_init();

    logger_dgb_print("[DS system] init done\n");
}

ds_state_t ds_go_down(void)
{
    static int laser_range_counter = 0;
    uint16_t range = piston_range_sensor_get_pos_mm();

#if (DS_DIVING_MICROSWITCH_USE == 1)
    static int switch_counter = 0;
    int switch_pressed = ds_is_diving_switch_triggered();
#else
    int switch_pressed = 0;
#endif

    if (ds_state == DS_SURFACING_OP_EXECUTION) {
        return DS_SURFACING_OP_EXECUTION;
    }

    if (range >= DS_SYRINGE_PISTON_MAX_OPEN_DIVING_STATE_MM || switch_pressed) {
        ds_servo_neutral_point();
        ds_state = DS_ON_BOTTOM;
        return DS_ON_BOTTOM;
    }

    ds_servo_rotate_cw(DS_SERVO_DIVING_SPEED_PROCENTS);
    ds_state = DS_DIVING_OP_EXECUTION;

    if (range >= DS_SYRINGE_PISTON_MAX_OPEN_DIVING_STATE_MM) {
        ++laser_range_counter;
    }
    if (switch_pressed) {
        ++switch_counter;
    }

    /* Likely we have reached pistons maximum position */
    if (laser_range_counter >= CNT_SAME_RANGE_VALUES || switch_counter >= SWITCH_MAX_COUNTER) {
        ds_servo_neutral_point();

        ds_state = DS_ON_BOTTOM;
        laser_range_counter = 0;
        switch_counter = 0;
    }

    return ds_state;
}

ds_state_t ds_go_up(void)
{
    static int laser_range_counter = 0;
    uint16_t range = piston_range_sensor_get_pos_mm();

#if (DS_SURFACING_MICROSWITCH_USE == 1)
    static int switch_counter = 0;
    int switch_pressed = ds_is_surfacing_switch_triggered();
#else
    int switch_pressed = 0;
#endif

    if (ds_state == DS_DIVING_OP_EXECUTION) {
        return DS_DIVING_OP_EXECUTION;
    }

    if (range <= DS_SYRINGE_PISTON_MIN_CLOSED_SURFACING_STATE_MM || switch_pressed) {
        ds_servo_neutral_point();
        ds_state = DS_ON_SURFACE;
        return DS_ON_SURFACE;
    }

    ds_servo_rotate_ccw(DS_SERVO_SURFACING_SPEED_PROCENTS);
    ds_state = DS_SURFACING_OP_EXECUTION;

    if (range <= DS_SYRINGE_PISTON_MIN_CLOSED_SURFACING_STATE_MM) {
        ++laser_range_counter;
    }
    if (switch_pressed) {
        ++switch_counter;
    }

    /* Likely we have reached pistons minimum position */
    if (laser_range_counter >= CNT_SAME_RANGE_VALUES || switch_counter >= SWITCH_MAX_COUNTER) {
        ds_servo_neutral_point();

        ds_state = DS_ON_SURFACE;
        laser_range_counter = 0;
        switch_counter = 0;
    }

    return ds_state;
}

ds_state_t ds_get_state(void)
{
    return ds_state;
}

void ds_stop_any_operations(void)
{
    ds_servo_neutral_point();
    ds_state = DS_UNKNOWN;
}
