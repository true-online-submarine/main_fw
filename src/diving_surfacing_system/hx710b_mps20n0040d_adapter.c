#include "hx710b_mps20n0040d_adapter.h"
#include "hx710b.h"
#include "diving_surfacing_system_config.h"
#include "main.h"
#include "sub_errors.h"
#include "sub_logger.h"
#include "non_os_delay.h"

static hx710b_dev_t pressure_adc = { 0 };
static uint32_t water_pressure_depth = 0;

static void clk_pin_reset(void)
{
    HAL_GPIO_WritePin(PRESSURE_SENS_SCK_GPIO_Port, PRESSURE_SENS_SCK_Pin, GPIO_PIN_RESET);
}

static void clk_pin_set(void)
{
    HAL_GPIO_WritePin(PRESSURE_SENS_SCK_GPIO_Port, PRESSURE_SENS_SCK_Pin, GPIO_PIN_SET);
}

static int dout_pin_read(void)
{
    if (HAL_GPIO_ReadPin(PRESSURE_SENS_OUT_GPIO_Port, PRESSURE_SENS_OUT_Pin) == GPIO_PIN_SET) {
        return 1;
    }

    return 0;
}

void water_pressure_sensor_adc_init(void)
{
    pressure_adc.clk_pin_reset = clk_pin_reset;
    pressure_adc.clk_pin_set = clk_pin_set;
    pressure_adc.delay_us = no_os_delay_us;
    pressure_adc.dout_pin_read = dout_pin_read;

    hx710b_power_on(&pressure_adc);
    hx710b_set_rate(&pressure_adc, HX710B_RATE_10Hz);

    logger_dgb_print("[DS system] HX710B-MPS20N0040D water pressure sensor init done\n");
}

void water_pressure_sensor_process(void)
{
    if (hx710b_is_new_data_available(&pressure_adc)) {
        water_pressure_depth = hx710b_read_raw_data(&pressure_adc);
    }
}

uint32_t water_pressure_get_depth_cm(void)
{
    return water_pressure_depth;
}
