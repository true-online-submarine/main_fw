#include "depth_laser_sensor_adapter.h"
#include "diving_surfacing_system_config.h"
#include "main.h"
#include "tf_mini_s.h"
#include "sub_errors.h"
#include "sub_logger.h"

#include <stdint.h>

#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS)

extern UART_HandleTypeDef huart1;

#if (DS_TFMINIS_USE_UART_IRQ == 1)
static uint8_t rx_byte = 0;
#endif

#if (DS_TFMINIS_USE_DMA_IDLE_IRQ == 1)
extern DMA_HandleTypeDef hdma_usart1_rx;
static uint8_t tfminis_data_frame[9] = { 0 };
#endif

static tfminis_ll_t depth_sensor_ll = { 0 };
static tfminis_dev_t depth_sensor_dev = { 0 };

#define DEPTH_SENSOR_SUCCESS 0
#define DEPTH_SENSOR_FAIL 1

static int tfminis_uart_send(uint8_t *data, size_t len)
{
    if (HAL_UART_Transmit(&huart1, data, len, 100) != HAL_OK) {
        return DEPTH_SENSOR_FAIL;
    }

    return DEPTH_SENSOR_SUCCESS;
}

static int tfminis_uart_recv(uint8_t *data, size_t len)
{
    if (HAL_UART_Receive(&huart1, data, len, 100) != HAL_OK) {
        return DEPTH_SENSOR_FAIL;
    }

    return DEPTH_SENSOR_SUCCESS;
}

static int tfminis_start_dma_or_irq_operations(void)
{
#if (DS_TFMINIS_USE_UART_IRQ == 1)
    if (HAL_UART_Receive_IT(&huart1, &rx_byte, 1) != HAL_OK) {
        return DEPTH_SENSOR_FAIL;
    }
#endif

#if (DS_TFMINIS_USE_DMA_IDLE_IRQ == 1)
    if (HAL_UARTEx_ReceiveToIdle_DMA(&huart1, tfminis_data_frame, sizeof(tfminis_data_frame)) != HAL_OK) {
        return DEPTH_SENSOR_FAIL;
    }
    __HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
#endif

    return DEPTH_SENSOR_SUCCESS;
}

static int tfminis_stop_dma_or_irq_operations(void)
{
#if (DS_TFMINIS_USE_UART_IRQ == 1 || DS_TFMINIS_USE_DMA_IDLE_IRQ == 1)
    if (HAL_UART_Abort(&huart1) != HAL_OK) {
        return DEPTH_SENSOR_FAIL;
    }
#endif

    return DEPTH_SENSOR_SUCCESS;
}

static int new_data_availavle = 0;
void tfminis_data_avaliabe_isr_cb(void)
{
    new_data_availavle = 1;
}

void depth_tof_lidar_sensor_init(void)
{
    tfminis_ret_t ret = TFMINIS_FAIL;

    depth_sensor_ll.delay_ms = HAL_Delay;
    depth_sensor_ll.uart_send = tfminis_uart_send;
    depth_sensor_ll.uart_recv = tfminis_uart_recv;
    depth_sensor_ll.start_dma_or_irq_operations = tfminis_start_dma_or_irq_operations;
    depth_sensor_ll.stop_dma_or_irq_operations = tfminis_stop_dma_or_irq_operations;
    depth_sensor_ll.data_avaliabe_isr_cb = tfminis_data_avaliabe_isr_cb;
    depth_sensor_dev.ll = &depth_sensor_ll;

    ret = tfminis_init(&depth_sensor_dev, TFMINIS_USE_UART);
    if (ret != TFMINIS_OK) {
        logger_dgb_print("[Submarine ERR] TFmini-S code = %d\n", ret);
    }
    SUB_ASSERT(ret != TFMINIS_OK, SUB_TF_MINI_S_ASSERT_105, SUB_ASSERT_CRITICAL);

    ret = tfminis_set_frame_rate(&depth_sensor_dev, DS_TFMINIS_INPUT_DATA_FRAME_RATE_HZ);
    SUB_ASSERT(ret != TFMINIS_OK, SUB_TF_MINI_S_ASSERT_106, SUB_ASSERT_CRITICAL);

    logger_dgb_print("[DS system] TFmini-S laser depth sensor init done. V%d.%d.%d\n",
                     (uint8_t)((depth_sensor_dev.fw_version & 0xff0000) >> 16),
                     (uint8_t)((depth_sensor_dev.fw_version & 0xff00) >> 8),
                     (uint8_t)depth_sensor_dev.fw_version);
}

tof_sensor_data_t depth_tof_lidar_get_distance_cm(void)
{
    tof_sensor_data_t d;

    d.distanse_cm = tfminis_get_distance(&depth_sensor_dev).distance_cm;
    d.err_reason = (uint16_t)tfminis_get_err_reason(&depth_sensor_dev);

    return d;
}

uint16_t depth_tof_lidar_get_temp(void)
{
    return tfminis_get_chip_temp(&depth_sensor_dev);
}

int depth_tof_lidar_is_new_data_available(void)
{
    int temp = new_data_availavle;
    new_data_availavle = 0;

    return temp;
}

void depth_tof_lidar_uart_irq_handler(void)
{
#if (DS_TFMINIS_USE_UART_IRQ == 1)
        tfminis_handle_rx_byte_uart_isr(&depth_sensor_dev, rx_byte);
        HAL_UART_Receive_IT(&huart1, &rx_byte, 1);
#endif
}

void depth_tof_lidar_dma_irq_handler(size_t rx_dma_byte_cnt)
{
#if (DS_TFMINIS_USE_DMA_IDLE_IRQ == 1)
    tfminis_handle_rx_data_dma_isr(&depth_sensor_dev, tfminis_data_frame, rx_dma_byte_cnt);
    HAL_UARTEx_ReceiveToIdle_DMA(&huart1, tfminis_data_frame, sizeof(tfminis_data_frame));
    __HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
#endif
}

#endif /* DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS */
