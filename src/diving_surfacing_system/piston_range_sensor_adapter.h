#ifndef __PISTON_RANGE_SENSOR_ADAPTER_H
#define __PISTON_RANGE_SENSOR_ADAPTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void piston_range_sensor_init(void);
void piston_range_sensor_process(void); /* call this in event handler thread */
void piston_range_sensor_isr(void); /* call this in IRQ handler */
uint16_t piston_range_sensor_get_pos_mm(void);
uint16_t piston_range_sensor_get_oneshot_pos_mm(void);

#ifdef __cplusplus
}
#endif

#endif /* __PISTON_RANGE_SENSOR_ADAPTER_H */
