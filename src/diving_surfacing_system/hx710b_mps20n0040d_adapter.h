#ifndef __HX710B_MPS20N0040D_ADAPTER_H
#define __HX710B_MPS20N0040D_ADAPTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void water_pressure_sensor_adc_init(void);
void water_pressure_sensor_process(void);
uint32_t water_pressure_get_depth_cm(void);

#ifdef __cplusplus
}
#endif

#endif /* __HX710B_MPS20N0040D_ADAPTER_H */
