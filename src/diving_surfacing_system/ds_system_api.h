#ifndef __DS_SYSTEM_API_H
#define __DS_SYSTEM_API_H

typedef enum {
    DS_UNKNOWN,
    DS_ON_BOTTOM,
    DS_ON_SURFACE,
    DS_DIVING_OP_EXECUTION,
    DS_SURFACING_OP_EXECUTION,
} ds_state_t;

void ds_init(void);

ds_state_t ds_go_down(void);
ds_state_t ds_go_up(void);
ds_state_t ds_get_state(void);
void ds_stop_any_operations(void);

#endif /* __DS_SYSTEM_API_H */
