#ifndef __DIVING_SURFACING_SYSTEM_CONFIG_H
#define __DIVING_SURFACING_SYSTEM_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(DS_SYSTEM_BASED_ON_SYRINGE)

/* Mechanics */
# define DS_SYRINGE_COUNTS 4
# define DS_SYRINGE_VOLUME_ML 150
# define DS_SYRINGE_PISTON_MAX_OPEN_DIVING_STATE_MM (80UL)
# define DS_SYRINGE_PISTON_MIN_CLOSED_SURFACING_STATE_MM (10UL)

/* Use logger during diving\surfacing? */
# define DS_SYSTEM_USE_LOGGER 1
# define DS_SYSTEM_LOGGING_PERIOD_MS (1000UL)

/* Data about servo from datasheet */
# define DS_SERVO_PWM_FREQ_HZ 50
# define DS_SERVO_NEUTRAL_POINT_PWM_US 1500
# define DS_SERVO_MAX_CW_US 500
# define DS_SERVO_MAX_CCW_US 2500

/* Setting for your setup */
# define DS_SERVO_DIVING_SPEED_PROCENTS 100
# define DS_SERVO_SURFACING_SPEED_PROCENTS 100

/* Use unsafe debug commands for diving system? */
# define DS_DEBUG_USE_UNSAFE_COMMANDS 1
# define DS_SERVO_UNSAFE_SERVO_CCW_SPEED 40
# define DS_SERVO_UNSAFE_SERVO_CW_SPEED 40

/* Define how to detect piston position */
# if defined(DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH)
#  define DS_DIVING_MICROSWITCH_USE 1
#  define DS_SURFACING_MICROSWITCH_USE 1
# endif
# if defined(DS_SYSTEM_PISTON_DETECTOR_USE_VL53L0X)
#  define DS_VL53L0X_LOGGER_USE 1
#  define DS_VL53L0X_LOGGING_PERIOD_MS (1000UL)
#  define DS_VL53L0X_GETTING_DATA_PERIOD_MS (200UL)
# endif
# if !defined(DS_SYSTEM_PISTON_DETECTOR_USE_MICROSWITCH) && !defined(DS_SYSTEM_PISTON_DETECTOR_USE_VL53L0X)
#  error "How to detect piston position?"
# endif

#else
# error "Define and setup your diving surfacing system! DS_SYSTEM_BASED_ON_SYRINGE ?"
#endif

#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_LIDAR_TFMINIS)
# define DS_TFMINIS_LOGGER_USE 1
# define DS_TFMINIS_LOGGING_PERIOD_MS (1000UL)
# define DS_TFMINIS_INPUT_DATA_FRAME_RATE_HZ 10 /* How often the sensor will send data in case of using uart IRQ or DMA idle event IRQ */
# define DS_TFMINIS_USE_UART_IRQ 0
# define DS_TFMINIS_USE_DMA_IDLE_IRQ 1
#endif

#if defined(DS_SYSTEM_DEPTH_DETECTOR_USE_MPS20N0040D)
# define DS_MPS20N0040D_LOGGER_USE 1
# define DS_MPS20N0040D_LOGGING_PERIOD_MS (1000UL)
#endif

#if defined(DS_SYSTEM_USE_EMERGENCY_SURFACING)
#define DS_LED_BLINK_IF_EMERGENCY_SURFACING_DETECTED 1
#define DS_LED_BLINK_IF_EMERGENCY_SURFACING_PERIOD_MS 200
#define DS_EMERGENCY_SURFACING_AFTER_SECONDS 60
#endif

#ifdef __cplusplus
}
#endif

#endif /* __DIVING_SURFACING_SYSTEM_CONFIG_H */
