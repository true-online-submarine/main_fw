#ifndef __COMMON_SUB_LOGGER_CONF_H
#define __COMMON_SUB_LOGGER_CONF_H

#ifdef __cplusplus
extern "C" {
#endif

# ifndef SUB_LOGGER_BUF_SIZE
#  define SUB_LOGGER_BUF_SIZE 256
# endif

# ifndef SUB_LOGGER_UART_MODULE
#  error "Define SUB_LOGGER_UART_MODULE, please! For instance -DSUB_LOGGER_UART_MODULE=huart1."
# endif

#ifdef __cplusplus
}
#endif

#endif /* __COMMON_SUB_LOGGER_CONF_H */
