    #ifndef __THRUSTERS_CONFIG_H
#define __THRUSTERS_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#define THRUSTER_FEATURE_UNSUPPORTED 0xffffffff

/* Enumerate all thrusters in your submarine */
typedef enum {
    LEFT_MIDDLE_THRUSTER = 0,
    RIGH_MIDDLE_THRUSTER,

    MAX_THRUSTER_COUNTS
} thruster_position_t;

/* clockwise or counter clockwise */
typedef enum {
    THRUSTER_ROTATION_CW,
    THRUSTER_ROTATION_CCW,
    THRUSTER_ROTATION_NEUTRAL,
} thruster_dir_t;

#if defined(THRUSTERS_USE_BLDC_MOTORS)
# if defined(ESC_USE_JETI_MODEL_ADVANCED)
#  define ESC_MIN_DELAY_BEFORE_START_MS 2000
#  define ESC_MAX_PWM_US 2000
#  define ESC_MIN_PWM_US 1000
#  define ESC_NEUTRAL_POINT_PWM_US THRUSTER_FEATURE_UNSUPPORTED
#  define ESC_SW_BIDIRECTION_SUPPORT THRUSTER_FEATURE_UNSUPPORTED
#  define ESC_THRUSTERS_POWER_STEP_PERCENTS (25UL)
# elif defined(ESC_USE_ZMR_UNKNOWN_BIDIR)
#  define ESC_MIN_DELAY_BEFORE_START_MS 0
#  define ESC_MAX_PWM_US 2000
#  define ESC_MIN_PWM_US 1000
#  define ESC_NEUTRAL_POINT_PWM_US 1500
#  define ESC_SW_BIDIRECTION_SUPPORT 1
#  define ESC_THRUSTERS_POWER_STEP_PERCENTS (2UL)
# else
#  error "Define esc contoller type"
# endif

#elif defined (THRUSTERS_USE_DC_MOTORS)
# error "DS motors with H-bridge are unsupported for now."
#else
# error "Define the type of submarine thrusters: THRUSTERS_USE_BLDC_MOTORS or THRUSTERS_USE_DC_MOTORS"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __THRUSTERS_CONFIG_H */
