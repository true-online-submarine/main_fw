#include "main.h"
#include "peripheral_init.h"
#include "sub_logger.h"
#include "sub_errors.h"

#include "light_api.h"
#include "ds_system_api.h"
#include "thruster_api.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"

#include "non_os_delay.h"

#include "sensors_data_collector.h"
#include "banana_message_dispatcher.h"
#include "diving_system.h"
#include "rf433_dispatcher.h"

static void hello_blink(void)
{
#define HELLO_BLINK_CNT 2
#define HELLO_BLINK_DELAY 100

    for (int i = 0; i < HELLO_BLINK_CNT; ++i) {
        light_brightness_max();
        HAL_Delay(HELLO_BLINK_DELAY);
        light_brightness_min();
        HAL_Delay(HELLO_BLINK_DELAY);
    }
}

int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    clock_init();

    /* FreeRTOS will setup SysTick by itself.
       We get HardFault when SysTick IRQ has happened before the scheduler started.
       So, we MUST disable it. Otherwise 1ms probably elapsed if we use uart logger
       function for instance and we got HF.
    */
    systick_disable_int();

    /* No OS delay to make HAL_Delay() alive and init counter for 1ms less delays */
    no_os_delay_timer_init_irq();

    /* Initialize all configured peripherals */
    gpio_init();
    i2c1_init();
    tim2_init();
    tim5_init();
    tim9_init();
    uart1_init();
    uart6_init();
    dma2_stream2_init();
    tim3_init();

    /* Init left and right thruster PWM and set duty to 0% */
    thrusters_init();

    /* Init diving surfacing system */
    ds_init();

    /* Start PWM LED timer and set duty to 0% */
    light_init();

    /* RTOS thread to collect data from different sensors.
       Triggers SUB_SENSORS_DATA_COLLECTOR_THREAD_ASSERT_1000 if creation failed */
    sensors_data_collector_thread_create();

    /* RTOS thread to manage diving system */
    ds_system_thread_create();

    /* RTOS thread and queue to read, dispatch and execute commands from Linux main board */
    banana_message_dispatcher_thread_create();

    /* RTOS thread and queue to listen, dispatch and execute commands from RF433 reveiver module (rx470c-v01) */
    rf433_dispatcher_thread_create();

    /* This "Hello" from the FW means that all necessary initializations successfully done.
       Otherwise we got some messages from SUB_ASSERT() and than system reset.
       Check the sub_error.h module in common repo. (This module is included by PlatformIO) */
    logger_dgb_print("[Submarine] FW started. CPU clock = %d Hz\n", SystemCoreClock);
    hello_blink();

    /* Disable 1ms IRQ for hal_delay, because after OS start, we use vApplicationTickHook() for inc tick */
    no_os_delay_timer_disable_irq();

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* Will only get here if there was insufficient memory to create the idle
    task. The idle task is created within vTaskStartScheduler().
    We use static allocation, but leave it here for the future */
    for( ;; ) {
        logger_dgb_print("[Submarine ERR] insufficient memory!\n");
        HAL_GPIO_TogglePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin);
        HAL_Delay(100);
    }
}
