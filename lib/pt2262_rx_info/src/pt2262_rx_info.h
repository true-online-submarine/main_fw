#ifndef __PT2262_RX_INFO_H
#define __PT2262_RX_INFO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
    uint16_t edge_h;
    uint16_t edge_l;
} pt2262_half_bit_edges_t;

typedef enum {
    RF_WRONG_PULSE, /* in case if we receive RF noise or just not our protocol */
    RF_WRONG_ADDR, /* all pulses is OK, but it is not our device address */
    RF_WRONG_CMD,
    RF_WAIT_NEXT_PULSE,
    RF_CMD_RECEIVED, /* All passed successfully and we obtained a command to execute */
} pt2262_parser_state_t;

/*
 * The function will parse data pulse by pulse to find out RF address and command.
 * 
 * Returns pt2262_parser_state_t values.
 * When RF_CMD_RECEIVED value is returned, the command is placed in ret_cmd pointer
 **/
int pt2262_rx_data_parser(pt2262_half_bit_edges_t *half_bit, uint8_t *ret_cmd);

#ifdef __cplusplus
}
#endif

#endif /* __PT2262_RX_INFO_H */
