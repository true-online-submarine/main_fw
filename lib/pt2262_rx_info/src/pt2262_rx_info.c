#include "main.h"
#include "pt2262_rx_info.h"
#include "sub_logger.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

enum parser_state_t {
    ADDR_RX_IN_PROGRESS,
    CMD_RX_IN_PROGRESS,
};

enum bit_rx_state_t {
    BIT_RX_FIRST_PART,
    BIT_RX_SECOND_PART,
};

typedef struct {
    uint16_t edge_h1;
    uint16_t edge_l1;
    uint16_t edge_h2;
    uint16_t edge_l2;
} pt2262_edges_item_t;

static int pulse_is_0(pt2262_edges_item_t *edges);
static int pulse_is_1(pt2262_edges_item_t *edges);
static int pulse_is_t(pt2262_edges_item_t *edges);
static void reset_parser(void);
static int pulse_is_sync(uint16_t h, uint16_t l);

static enum parser_state_t parser_state = ADDR_RX_IN_PROGRESS;
static enum bit_rx_state_t bit_rx_state = BIT_RX_FIRST_PART;

#define DEBUG_BIT_PATTERNS 0

#define CMD_BIT_CNT 3
static uint8_t our_real_addr_bits[] = { '0', '0', '1', 'T', '1', 'T', '0', '0', '1' };
static uint8_t rx_addr_bits[sizeof(our_real_addr_bits)] = { 0 };
static uint8_t rx_command = 0;
static size_t rx_bit_count = 0;

int pt2262_rx_data_parser(pt2262_half_bit_edges_t *half_bit, uint8_t *ret_cmd)
{
    static pt2262_edges_item_t edges = { 0 };

    if (pulse_is_sync(half_bit->edge_h, half_bit->edge_l)) {
        reset_parser();
        return RF_WAIT_NEXT_PULSE;
    } else {
        if (bit_rx_state == BIT_RX_FIRST_PART) {
            edges.edge_h1 = half_bit->edge_h;
            edges.edge_l1 = half_bit->edge_l;
            bit_rx_state = BIT_RX_SECOND_PART;

            return RF_WAIT_NEXT_PULSE;
        } else {
            edges.edge_h2 = half_bit->edge_h;
            edges.edge_l2 = half_bit->edge_l;
            bit_rx_state = BIT_RX_FIRST_PART;
        }
    }

    if (parser_state == ADDR_RX_IN_PROGRESS) {
        if (pulse_is_0(&edges)) {
            rx_addr_bits[rx_bit_count++] = '0';
            #if DEBUG_BIT_PATTERNS
                logger_dgb_print("It's 0\n");
            #endif
        } else if (pulse_is_1(&edges)) {
            rx_addr_bits[rx_bit_count++] = '1';
            #if DEBUG_BIT_PATTERNS
                logger_dgb_print("It's 1\n");
            #endif
        } else if (pulse_is_t(&edges)) {
            rx_addr_bits[rx_bit_count++] = 'T';
            #if DEBUG_BIT_PATTERNS
                logger_dgb_print("It's T\n");
            #endif
        } else {
            reset_parser();
            return RF_WRONG_PULSE;
        }

        /* Address received? */
        if (rx_bit_count == sizeof(our_real_addr_bits)) {
            if (memcmp((const void *)rx_addr_bits,
                       (const void *)our_real_addr_bits,
                       sizeof(our_real_addr_bits)) == 0) {
                parser_state = CMD_RX_IN_PROGRESS;
                rx_bit_count = 0;
                #if 0
                logger_dgb_print("[RF433] device addr is %c%c%c%c%c%c%c%c%c\n",
                                 rx_addr_bits[0], rx_addr_bits[1], rx_addr_bits[2],
                                 rx_addr_bits[3], rx_addr_bits[4], rx_addr_bits[5],
                                 rx_addr_bits[6], rx_addr_bits[7], rx_addr_bits[8]);
                #endif
            } else {
                reset_parser();
                return RF_WRONG_ADDR;
            }
        }
    } else if (parser_state == CMD_RX_IN_PROGRESS) {
        if (pulse_is_0(&edges)) {
            ++rx_bit_count;
        } else if (pulse_is_1(&edges)) {
            rx_command |= (1 << (CMD_BIT_CNT - rx_bit_count - 1));
            ++rx_bit_count;
        } else {
            reset_parser();
            #if 0
            logger_dgb_print("[RF433] wrong cmd\n");
            #endif
            return RF_WRONG_CMD;
        }

        /* Command received? */
        if (rx_bit_count == CMD_BIT_CNT) {
            if (ret_cmd != NULL) {
                *ret_cmd = rx_command;
            }

            reset_parser();
            return RF_CMD_RECEIVED;
        }
    }

    return RF_WAIT_NEXT_PULSE;
}

static int pulse_is_sync(uint16_t h, uint16_t l)
{
    if (h >= 500 && h <= 700 && l >= 10000) {
        return 1;
    }

    return 0;
}

/* Appropriate to logic zero pattern? */
static int pulse_is_0(pt2262_edges_item_t *edges)
{
    if (edges->edge_h1 >= 500 && edges->edge_h1 <= 800 && \
        edges->edge_l1 >= 1300 && edges->edge_l1 <= 1600 && \
        edges->edge_h2 >= 500 && edges->edge_h2 <= 800 && \
        edges->edge_l2 >= 1300 && edges->edge_l2 <= 1600) {
        return 1;
    }

#if DEBUG_BIT_PATTERNS
    logger_dgb_print("NOT 0: %d %d %d %d\n", edges->edge_h1, edges->edge_l1, edges->edge_h2, edges->edge_l2);
#endif
    return 0;
}

/* Appropriate to logic one pattern? */
static int pulse_is_1(pt2262_edges_item_t *edges)
{
    if (edges->edge_h1 >= 1500 && edges->edge_h1 <= 1800 && \
        edges->edge_l1 >= 350 && edges->edge_l1 <= 550 && \
        edges->edge_h2 >= 1500 && edges->edge_h2 <= 1800 && \
        edges->edge_l2 >= 350 && edges->edge_l2 <= 550) {
        return 1;
    }

#if DEBUG_BIT_PATTERNS
    logger_dgb_print("NOT 1: %d %d %d %d\n", edges->edge_h1, edges->edge_l1, edges->edge_h2, edges->edge_l2);
#endif
    return 0;
}

/* Appropriate to tri-state pattern? */
static int pulse_is_t(pt2262_edges_item_t *edges)
{
    if (edges->edge_h1 >= 500 && edges->edge_h1 <= 750 && \
        edges->edge_l1 >= 1380 && edges->edge_l1 <= 1580 && \
        edges->edge_h2 >= 1580 && edges->edge_h2 <= 1780 && \
        edges->edge_l2 >= 380 && edges->edge_l2 <= 580) {
        return 1;
    }

#if DEBUG_BIT_PATTERNS
    logger_dgb_print("NOT T: %d %d %d %d\n", edges->edge_h1, edges->edge_l1, edges->edge_h2, edges->edge_l2);
#endif
    return 0;
}

static void reset_parser(void)
{
    rx_command = 0;
    rx_bit_count = 0;
    parser_state = ADDR_RX_IN_PROGRESS;
    bit_rx_state = BIT_RX_FIRST_PART;
}
