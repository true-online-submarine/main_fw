#ifndef __BANANA_LINK_H
#define __BANANA_LINK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum {
    LIGHT_SUBSYSTEM,
    THRUSTERS_SUBSYSTEM,
    DS_SUBSYSTEM,
} banana_link_subsystem_t;

/* Light subsystem commands */
#define LIGHT_ON_CMD      1
#define LIGHT_OFF_CMD     2
#define LIGHT_SET_VAL_CMD 3 /* used with 'value' param */
#define LIGHT_BRIGHTNESS_STET_UP 4
#define LIGHT_BRIGHTNESS_STET_DOWN 5

/* Thrusters subsystem commands */
#define THRUSTERS_SWIM_FORWARD_CMD      1 /* enable thrusters with 10% power to swim forward */
#define THRUSTERS_SWIM_BACKWARD_CMD     2 /* enable thrusters with 10% power to swim backward */
#define THRUSTERS_OFF_CMD               3 /* all off */
#define THRUSTERS_LEFT_POWER_STEP_UP    4
#define THRUSTERS_LEFT_POWER_STEP_DOWN  5
#define THRUSTERS_RIGHT_POWER_STEP_UP   6
#define THRUSTERS_RIGHT_POWER_STEP_DOWN 7

typedef struct __attribute__((packed)) {
    uint8_t system;
    uint8_t command;
    uint8_t value;
} banana_message_t;

void banana_message_parse_execute(banana_message_t *msg);

#ifdef __cplusplus
}
#endif

#endif /* __BANANA_LINK_H */
