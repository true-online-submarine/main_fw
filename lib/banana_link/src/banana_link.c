#include "banana_link.h"
#include "light_api.h"
#include "ds_system_api.h"
#include "thruster_api.h"
#include "ds_servo.h"
#include "diving_system.h"
#include "sub_logger.h"

#define LIGHT_BRIGHTNESS_STEP_PERCENTS (10UL)

static void handle_light_command(banana_message_t *msg)
{
    switch (msg->command) {
    case LIGHT_ON_CMD:
        light_brightness_max();
        break;
    case LIGHT_OFF_CMD:
        light_brightness_min();
        break;
    case LIGHT_SET_VAL_CMD:
        light_set_brightness(msg->value);
        break;
    case LIGHT_BRIGHTNESS_STET_UP:
        light_set_brightness(light_get_cur_brightness() + LIGHT_BRIGHTNESS_STEP_PERCENTS);
        break;
    case LIGHT_BRIGHTNESS_STET_DOWN:
        light_set_brightness(light_get_cur_brightness() - LIGHT_BRIGHTNESS_STEP_PERCENTS);
        break;
    default:
        logger_dgb_print("[dispatcher] unknown light command\n");
        break;
    }
}

static void handle_thrusters_command(banana_message_t *msg)
{
    switch (msg->command) {
    case THRUSTERS_SWIM_FORWARD_CMD:
        thrusters_swim_forward(10);
        break;
    case THRUSTERS_SWIM_BACKWARD_CMD:
        thrusters_swim_backward(10);
        break;
    case THRUSTERS_OFF_CMD:
        thrusters_stop_all();
        break;
    case THRUSTERS_LEFT_POWER_STEP_UP:
        thrusters_set_speed(LEFT_MIDDLE_THRUSTER, msg->value, thrusters_get_speed(LEFT_MIDDLE_THRUSTER) + ESC_THRUSTERS_POWER_STEP_PERCENTS);
        break;
    case THRUSTERS_LEFT_POWER_STEP_DOWN:
        thrusters_set_speed(LEFT_MIDDLE_THRUSTER, msg->value, thrusters_get_speed(LEFT_MIDDLE_THRUSTER) - ESC_THRUSTERS_POWER_STEP_PERCENTS);
        break;
    case THRUSTERS_RIGHT_POWER_STEP_UP:
        thrusters_set_speed(RIGH_MIDDLE_THRUSTER, msg->value, thrusters_get_speed(RIGH_MIDDLE_THRUSTER) + ESC_THRUSTERS_POWER_STEP_PERCENTS);
        break;
    case THRUSTERS_RIGHT_POWER_STEP_DOWN:
        thrusters_set_speed(RIGH_MIDDLE_THRUSTER, msg->value, thrusters_get_speed(RIGH_MIDDLE_THRUSTER) - ESC_THRUSTERS_POWER_STEP_PERCENTS);
        break;
    default:
        logger_dgb_print("[dispatcher] unknown thruster command\n");
        break;
    }
}

void banana_message_parse_execute(banana_message_t *msg)
{
    logger_dgb_print("[dispatcher] received message: system=%d, cmd=%d, val=%d\n",
                     msg->system, msg->command, msg->value);

    switch (msg->system) {
    case LIGHT_SUBSYSTEM:
        handle_light_command(msg);
        break;
    case THRUSTERS_SUBSYSTEM:
        handle_thrusters_command(msg);
        break;
    case DS_SUBSYSTEM:
        ds_system_start_execute_cmd((ds_commands_t)msg->command);
        break;
    default:
        logger_dgb_print("[dispatcher] unknown subsystem\n");
        break;
    }
}
